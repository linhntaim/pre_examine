﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace XmlImport
{
    public class SummaryReport
    {
        public int TotalFile;
        public int TotalFileValidated;
        public int TotalFileHasAnyError;
        public int TotalFileInternal;
        public int TotalFileExternal;

        public int TotalCost;
        public int TotalCostBhtt;
        public int TotalCostBntt;
        public int TotalCostOther;
        public int TotalCostDiff;

        public int TotalCostXml1;
        public int TotalCostXml23;
        public int TotalCostDiffXml;

        public int TotalPillCostXml1;
        public int TotalPillCostXml2;
        public int TotalPillCostDiff;

        public int TotalEquipCostXml1;
        public int TotalEquipCostXml3;
        public int TotalEquipCostDiff;

        public SummaryReport()
        {
            Reset();
        }

        public void Reset()
        {
            TotalFile = 0;
            TotalFileValidated = 0;
            TotalFileHasAnyError = 0;
            TotalFileInternal = 0;
            TotalFileExternal = 0;

            TotalCost = 0;
            TotalCostBhtt = 0;
            TotalCostBntt = 0;
            TotalCostOther = 0;
            TotalCostDiff = 0;

            TotalCostXml1 = 0;
            TotalCostXml23 = 0;
            TotalCostDiffXml = 0;

            TotalPillCostXml1 = 0;
            TotalPillCostXml2 = 0;
            TotalPillCostDiff = 0;

            TotalEquipCostXml1 = 0;
            TotalEquipCostXml3 = 0;
            TotalEquipCostDiff = 0;
        }
    }
}
