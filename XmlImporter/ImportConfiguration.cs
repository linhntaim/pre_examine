﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using XmlImport.Validation;

namespace XmlImport
{
    [Serializable]
    class ImportConfiguration
    {
        public Dictionary<string, string> HeaderIndicators;
        public RuleChain[] RuleChains;
        public string[] Meta;
        public Dictionary<string, string> Messages;
        public Settings Settings;
        public DateTime UpdateAt;
    }
}
