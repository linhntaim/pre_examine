﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;

namespace XmlImport.DataStructure
{
    public abstract class PatientXml
    {
        private string raw;

        public PatientXml(string content)
        {
            raw = content;
            XmlDocument xml = new XmlDocument();
            xml.LoadXml(content);
            Parse(xml);
        }

        public string GetRaw()
        {
            return raw;
        }

        protected abstract void Parse(XmlDocument xml);
        public abstract string[] GetData(string field, bool accessRaw = false);
    }
}
