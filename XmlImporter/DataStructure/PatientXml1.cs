﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;

namespace XmlImport.DataStructure
{
    class PatientXml1 : PatientXml
    {
        private Dictionary<string, string> data;
        private Dictionary<string, string> dataRaw;

        public PatientXml1(string content) : base(content)
        {
        }

        protected override void Parse(XmlDocument xml)
        {
            data = new Dictionary<string, string>();
            dataRaw = new Dictionary<string, string>();
            foreach (XmlNode node in xml.DocumentElement.ChildNodes)
            {
                data[node.Name.ToUpper()] = node.InnerText;
                dataRaw[node.Name.ToUpper()] = node.InnerXml;
            }
        }

        public override string[] GetData(string field, bool accessRaw = false)
        {
            Dictionary<string, string> _data = accessRaw ? dataRaw : data;

            string[] parts = field.ToUpper().Split('.');
            if (parts.Length != 2) return null;

            return _data.ContainsKey(parts[1]) ? new string[] { _data[parts[1]] } : new string[] { null };
        }
    }
}
