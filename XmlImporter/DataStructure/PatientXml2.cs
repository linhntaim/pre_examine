﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;

namespace XmlImport.DataStructure
{
    class PatientXml2 : PatientXml
    {
        private List<Dictionary<string, string>> data;
        private List<Dictionary<string, string>> dataRaw;

        public PatientXml2(string content) : base(content)
        {
        }

        protected override void Parse(XmlDocument xml)
        {
            data = new List<Dictionary<string, string>>();
            dataRaw = new List<Dictionary<string, string>>();
            foreach (XmlNode node in xml.DocumentElement.ChildNodes)
            {
                Dictionary<string, string> item = new Dictionary<string, string>();
                Dictionary<string, string> itemRaw = new Dictionary<string, string>();
                foreach (XmlNode childNode in node.ChildNodes)
                {
                    item[childNode.Name.ToUpper()] = childNode.InnerText;
                    itemRaw[childNode.Name.ToUpper()] = childNode.InnerXml;
                }
                data.Add(item);
                dataRaw.Add(item);
            }
        }

        public override string[] GetData(string field, bool accessRaw = false)
        {
            List<Dictionary<string, string>> _data = accessRaw ? dataRaw : data;

            string[] parts = field.ToUpper().Split('.');
            if (parts.Length != 3) return null;

            if (parts[1] == "*")
            {
                List<string> r = new List<string>();
                foreach (Dictionary<string, string> item in _data)
                {
                    r.Add(item[parts[2]]);
                }
                return r.ToArray();
            }

            try
            {
                int index = int.Parse(parts[1]);
                return new string[] { _data[index][parts[2]] };
            }
            catch
            {
                return new string[] { null };
            }
        }
    }
}
