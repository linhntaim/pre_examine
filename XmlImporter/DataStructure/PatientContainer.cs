﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;

namespace XmlImport.DataStructure
{
    public class PatientDataContainer
    {
        protected string fileName;
        protected Dictionary<string, PatientXml> files;

        public PatientDataContainer(string fileName)
        {
            this.fileName = fileName;
            files = new Dictionary<string, PatientXml>();
        }

        public string GetFileName()
        {
            return this.fileName;
        }

        public bool HasFile(string type)
        {
            return files.ContainsKey(type.ToUpper());
        }

        public void PushFile(string type, string content)
        {
            type = type.ToUpper();

            switch (type)
            {
                case "XML1":
                    files[type] = new PatientXml1(content);
                    break;
                case "XML2":
                    files[type] = new PatientXml2(content);
                    break;
                case "XML3":
                    files[type] = new PatientXml3(content);
                    break;
            }
        }

        public string[] GetFieldData(string field, bool accessRaw = false)
        {
            string[] parts = field.ToUpper().Split('.');
            string fileType = parts[0];
            if (!files.ContainsKey(fileType)) return new string[] { null };
            return files[fileType].GetData(field, accessRaw);
        }

        public Dictionary<string, string> GetXml()
        {
            Dictionary<string, string> xml = new Dictionary<string, string>();
            foreach (KeyValuePair<string, PatientXml> p in files)
            {
                xml[p.Key] = p.Value.GetRaw();
            }
            return xml;
        }
    }
}
