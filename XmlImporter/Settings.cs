﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace XmlImport
{
    [Serializable]
    public class Settings
    {
        public string HospitalCode;
        public string HospitalName;
        public string HospitalAddress;
        public string CsKcbCode;

        public Settings()
        {
            HospitalAddress = HospitalCode = HospitalName = CsKcbCode = "";
        }
    }
}
