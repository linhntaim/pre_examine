﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using XmlImport.DataStructure;
using XmlImport.Validation.State;

namespace XmlImport.Validation
{
    class RuleLoader
    {
        private Dictionary<string, IRule> map;

        public RuleLoader()
        {
            map = new Dictionary<string, IRule>();
        }

        public void Load()
        {
            string[] dllFileNames = new string[] { };
            string dllPath = AppDomain.CurrentDomain.BaseDirectory + "/rules";
            if (Directory.Exists(dllPath))
            {
                dllFileNames = Directory.GetFiles(dllPath, "*.dll");
            }

            if (dllFileNames.Length > 0)
            {
                ICollection<Assembly> assemblies = new List<Assembly>(dllFileNames.Length);
                foreach (string dllFile in dllFileNames)
                {
                    AssemblyName an = AssemblyName.GetAssemblyName(dllFile);
                    Assembly assembly = Assembly.Load(an);
                    assemblies.Add(assembly);
                }

                Type ruleType = typeof(IRule);
                ICollection<Type> ruleTypes = new List<Type>();
                foreach (Assembly assembly in assemblies)
                {
                    if (assembly != null)
                    {
                        Type[] types = assembly.GetTypes();
                        foreach (Type type in types)
                        {
                            if (type.IsInterface || type.IsAbstract)
                            {
                                continue;
                            }
                            else
                            {
                                if (type.GetInterface(ruleType.FullName) != null)
                                {
                                    ruleTypes.Add(type);
                                }
                            }
                        }
                    }
                }

                foreach (Type type in ruleTypes)
                {
                    IRule rule = (IRule)Activator.CreateInstance(type);
                    map[rule.Name] = rule;
                }
            }
        }

        public ValidateState Run(string fieldData, string fieldDataRaw, RuleOption ruleOption, PatientDataContainer patientDataContainer, int index)
        {
            string ruleName = ruleOption.RuleName;
            if (map.ContainsKey(ruleName))
            {
                return map[ruleName].Check(fieldData, fieldDataRaw, ruleOption, patientDataContainer, index);
            }

            return null;
        }
    }
}
