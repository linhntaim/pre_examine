﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace XmlImport.Validation.State
{
    public class NotValidated : ValidateState
    {
        public NotValidated(RuleOption ruleOption) : base(ruleOption)
        {
        }
    }
}
