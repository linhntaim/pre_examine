﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace XmlImport.Validation.State
{
    public abstract class ValidateState
    {
        private RuleOption ruleOption;

        public ValidateState() { }

        public ValidateState(RuleOption ruleOption)
        {
            this.ruleOption = ruleOption;
        }

        public RuleOption GetRuleOption()
        {
            return this.ruleOption;
        }
    }
}
