﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace XmlImport.Validation
{
    [Serializable]
    class RuleChain
    {
        private string field;
        private List<RuleOption> ruleOptions;

        public RuleChain(string field)
        {
            this.field = field;
            this.ruleOptions = new List<RuleOption>();
        }

        public void AddRuleOption(RuleOption ruleOption)
        {
            ruleOptions.Add(ruleOption);
        }

        public string GetField()
        {
            return this.field;
        }

        public RuleOption[] GetRuleOptions()
        {
            return ruleOptions.ToArray();
        }
    }
}
