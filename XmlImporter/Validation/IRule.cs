﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using XmlImport.DataStructure;
using XmlImport.Validation.State;

namespace XmlImport.Validation
{
    public interface IRule
    {
        string Name { get; }
        ValidateState Check(string fieldData, string fieldDataRaw, RuleOption ruleOption, PatientDataContainer patientDataContainer, int index);
    }
}
