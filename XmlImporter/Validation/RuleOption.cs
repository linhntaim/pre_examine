﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace XmlImport.Validation
{
    [Serializable]
    public class RuleOption
    {
        public const int LEVEL_NONE = 0;
        public const int LEVEL_ERROR = 1;
        public const int LEVEL_WARNING = 2;

        public const int ERROR_TYPE_VAS = 1;
        public const int ERROR_TYPE_WARNING = 2;
        public const int ERROR_TYPE_PAID = 3;

        private string name;
        private string error;
        private int level;
        private bool raw;
        private Dictionary<string, string> options;

        public Dictionary<string, string> Constants;

        public string RuleName
        {
            get
            {
                return name;
            }
        }

        public string RuleError
        {
            get
            {
                return error;
            }
        }

        public int RuleErrorType
        {
            get
            {
                return int.Parse(error.Split('.')[0]);
            }
        }

        public int RuleLevel
        {
            get
            {
                return level;
            }
        }

        public bool UseDataRaw
        {
            get
            {
                return raw;
            }
        }

        public RuleOption(string name, string error, int level, bool raw = false)
        {
            this.name = name;
            this.error = error;
            this.level = level;
            this.raw = raw;
            options = new Dictionary<string, string>();
            Constants = new Dictionary<string, string>();
        }

        public void Add(string name, string value)
        {
            options[name] = value;
        }

        public string Get(string name)
        {
            return options.ContainsKey(name) ? options[name] : null;
        }
    }
}
