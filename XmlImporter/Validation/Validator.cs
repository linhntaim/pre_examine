﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using XmlImport.DataStructure;
using XmlImport.Validation.State;

namespace XmlImport.Validation
{
    class Validator
    {
        private RuleLoader ruleLoader;
        private List<RuleChain> ruleChains;

        public Dictionary<string, string> Constants;

        public Validator()
        {
            ruleLoader = new RuleLoader();
            ruleLoader.Load();

            ruleChains = new List<RuleChain>();
            Constants = new Dictionary<string, string>();
        }

        public void AddRuleChain(RuleChain ruleChain)
        {
            ruleChains.Add(ruleChain);
        }

        public RuleChain[] GetRuleChains()
        {
            return ruleChains.ToArray();
        }

        public void SetRuleChains(RuleChain[] ruleChains)
        {
            this.ruleChains.AddRange(ruleChains);
        }

        public void ClearRuleChains()
        {
            this.ruleChains.Clear();
        }

        public ValidateState[] Check(PatientDataContainer patientDataContainer)
        {
            List<ValidateState> notValidated = new List<ValidateState>();
            foreach (RuleChain ruleChain in ruleChains)
            {
                notValidated.AddRange(CheckRuleChain(ruleChain, patientDataContainer));
            }

            return notValidated.ToArray();
        }

        private ValidateState[] CheckRuleChain(RuleChain ruleChain, PatientDataContainer patientDataContainer)
        {
            List<ValidateState> notValidated = new List<ValidateState>();

            string fieldName = ruleChain.GetField();
            string[] fieldDataHolder = patientDataContainer.GetFieldData(fieldName);
            string[] fieldDataRawHolder = patientDataContainer.GetFieldData(fieldName, true);

            int index = 0;
            foreach (string fieldData in fieldDataHolder)
            {
                foreach (RuleOption ruleOption in ruleChain.GetRuleOptions())
                {
                    ruleOption.Constants = Constants;

                    ValidateState validateState = ruleLoader.Run(fieldData, fieldDataRawHolder[index], ruleOption, patientDataContainer, index);
                    if (validateState is NotValidated || validateState is BlockValidated)
                    {
                        if (validateState is NotValidated)
                        {
                            notValidated.Add(validateState);
                        }
                        break;
                    }
                }
                ++index;
            }

            return notValidated.ToArray();
        }
    }
}
