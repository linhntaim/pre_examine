﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using XmlImport.DataStructure;
using XmlImport.Validation;
using XmlImport.Validation.State;
using Utils;
using System.Data;

namespace XmlImport
{
    public class Manager
    {
        private static string CONFIG_FILE = AppDomain.CurrentDomain.BaseDirectory + "/config.mem";
        private const string MA_LOAI_KCB_KHAM_BENH = "1";
        private const string MA_LOAI_KCB_NGOAI_TRU = "2";
        private const string MA_LOAI_KCB_NOI_TRU = "3";

        private List<PatientDataContainer> patientDataContainers;
        private Dictionary<int, ValidateState[]> notValidated;
        private Validator validator;
        private DataTable dtErrorVas;
        private DataTable dtErrorWarning;
        private DataTable dtErrorPaid;

        public SummaryReport Summary;
        private Dictionary<string, string> headerIndicators;
        private List<string> meta;
        private Dictionary<string, string> messages;

        public Settings Settings;
        public DateTime UpdateAt;

        public Manager()
        {
            validator = new Validator();
            notValidated = new Dictionary<int, ValidateState[]>();
            dtErrorVas = new DataTable();
            dtErrorWarning = new DataTable();
            dtErrorPaid = new DataTable();
            foreach (string column in GetErrorHeader())
            {
                dtErrorVas.Columns.Add(column);
                dtErrorWarning.Columns.Add(column);
                dtErrorPaid.Columns.Add(column);
            }
            headerIndicators = new Dictionary<string, string>();
            messages = new Dictionary<string, string>();
            patientDataContainers = new List<PatientDataContainer>();
            Summary = new SummaryReport();
            meta = new List<string>();
            Settings = new Settings();
            UpdateAt = DateTime.Now;
        }

        public DataTable GetErrorVas()
        {
            return dtErrorVas;
        }

        public DataTable GetErrorWarning()
        {
            return dtErrorWarning;
        }

        public DataTable GetErrorPaid()
        {
            return dtErrorPaid;
        }

        public string[] GetMeta()
        {
            return meta.ToArray();
        }

        public bool LoadConfiguration(string fileName = null)
        {
            try
            {
                headerIndicators = new Dictionary<string, string>();
                validator.ClearRuleChains();
                messages.Clear();

                if (!string.IsNullOrEmpty(fileName))
                {
                    XmlDocument xml = new XmlDocument();
                    xml.Load(fileName);
                    XmlNodeList headerNodes = xml.DocumentElement.SelectNodes("//Configuration/Headers/Header");
                    foreach (XmlNode headerNode in headerNodes) // Header
                    {
                        headerIndicators[headerNode.Attributes["field"].Value.ToUpper()] = headerNode.InnerText.Trim();
                    }

                    XmlNodeList fieldNodes = xml.DocumentElement.SelectNodes("//Configuration/Validation/Field");
                    foreach (XmlNode fieldNode in fieldNodes) // Field
                    {
                        RuleChain ruleChain = new RuleChain(fieldNode.Attributes["name"].Value.ToUpper());
                        foreach (XmlNode ruleNode in fieldNode.ChildNodes)
                        {
                            RuleOption ruleOption = new RuleOption(
                                ruleNode.Attributes["name"].Value.ToLower(),
                                ruleNode.Attributes["error"].Value,
                                int.Parse(ruleNode.Attributes["level"].Value),
                                ruleNode.Attributes["raw"] != null && ruleNode.Attributes["raw"].Value == "true");
                            foreach (XmlNode optionNode in ruleNode.ChildNodes)
                            {
                                ruleOption.Add(optionNode.Name.ToLower(), optionNode.InnerText);
                            }
                            ruleChain.AddRuleOption(ruleOption);
                        }
                        validator.AddRuleChain(ruleChain);
                    }

                    XmlNodeList messageNodes = xml.DocumentElement.SelectNodes("//Configuration/Messages/Message");
                    foreach (XmlNode messageNode in messageNodes) // Message
                    {
                        messages[messageNode.Attributes["id"].Value] = messageNode.InnerText;
                    }

                    SaveImportConfiguration();
                }
                else
                {
                    ReadImportConfiguration();
                }
            }
            catch (Exception ex)
            {
                return false;
            }

            return true;
        }

        public void SaveMeta(string metaName)
        {
            if (!meta.Contains(metaName))
            {
                meta.Add(metaName);

                SaveImportConfiguration();
            }
        }

        public void DeleteMeta(string metaName)
        {
            if (meta.Contains(metaName))
            {
                meta.RemoveAt(meta.IndexOf(metaName));
                SaveImportConfiguration();
            }
        }

        public void SaveImportConfiguration()
        {
            ImportConfiguration importConfiguration = new ImportConfiguration();
            importConfiguration.HeaderIndicators = headerIndicators;
            importConfiguration.RuleChains = validator.GetRuleChains();
            importConfiguration.Meta = meta.ToArray();
            importConfiguration.Messages = messages;
            importConfiguration.Settings = Settings;
            importConfiguration.UpdateAt = UpdateAt = DateTime.Now;
            FileSaver.WriteToBinaryFile(CONFIG_FILE, importConfiguration);

            UpdateConstants();
        }

        private void ReadImportConfiguration()
        {
            if (FileSaver.IsExist(CONFIG_FILE))
            {
                ImportConfiguration importConfiguration = FileSaver.ReadFromBinaryFile<ImportConfiguration>(CONFIG_FILE);
                headerIndicators = importConfiguration.HeaderIndicators;
                validator.SetRuleChains(importConfiguration.RuleChains);
                meta.AddRange(importConfiguration.Meta);
                messages = importConfiguration.Messages;
                Settings = importConfiguration.Settings;
                UpdateAt = importConfiguration.UpdateAt;
                UpdateConstants();
            }
        }

        private void UpdateConstants()
        {
            validator.Constants = new Dictionary<string, string>();
            validator.Constants["cskcb"] = Settings.CsKcbCode;
        }

        public void StartNewSession()
        {
            notValidated.Clear();
            dtErrorVas.Rows.Clear();
            dtErrorWarning.Rows.Clear();
            dtErrorPaid.Rows.Clear();
            patientDataContainers.Clear();
            Summary.Reset();
        }

        private string GetErrorMessage(string code)
        {
            return messages.ContainsKey(code) ? messages[code] : code;
        }

        public void Push(string fileName)
        {
            try
            {
                XmlDocument xml = new XmlDocument();
                xml.Load(fileName);

                XmlNode MACSKCB = xml.DocumentElement.SelectSingleNode("//THONGTINDONVI/MACSKCB");
                if (MACSKCB.InnerText.Trim() == "")
                {
                    dtErrorVas.Rows.Add(new string[] {
                        fileName,
                        "",
                        "",
                        GetErrorMessage("1.2")
                    });
                }
                if (MACSKCB.InnerText != Settings.CsKcbCode)
                {
                    dtErrorVas.Rows.Add(new string[] {
                        fileName,
                        "",
                        "",
                        GetErrorMessage("1.3")
                    });
                }

                XmlNode NGAYLAP = xml.DocumentElement.SelectSingleNode("//THONGTINHOSO/NGAYLAP");
                if (NGAYLAP.InnerText.Trim() == "")
                {
                    dtErrorVas.Rows.Add(new string[] {
                        fileName,
                        "",
                        "",
                        GetErrorMessage("1.4")
                    });
                }
                try
                {
                    DateTime.ParseExact(NGAYLAP.InnerText.Trim(), "yyyyMMdd", null);
                }
                catch
                {
                    dtErrorVas.Rows.Add(new string[] {
                        fileName,
                        "",
                        "",
                        GetErrorMessage("1.5")
                    });
                }

                XmlNode SOLUONGHOSO = xml.DocumentElement.SelectSingleNode("//THONGTINHOSO/SOLUONGHOSO");
                if (SOLUONGHOSO.InnerText.Trim() == "")
                {
                    dtErrorVas.Rows.Add(new string[] {
                        fileName,
                        "",
                        "",
                        GetErrorMessage("1.6")
                    });
                }
                if (int.Parse(SOLUONGHOSO.InnerText.Trim()).ToString() != SOLUONGHOSO.InnerText.Trim())
                {
                    dtErrorVas.Rows.Add(new string[] {
                        fileName,
                        "",
                        "",
                        GetErrorMessage("1.7")
                    });
                }

                XmlNodeList nodeList = xml.DocumentElement.SelectNodes("//THONGTINHOSO/DANHSACHHOSO/HOSO");

                if (int.Parse(SOLUONGHOSO.InnerText.Trim()) != nodeList.Count)
                {
                    dtErrorVas.Rows.Add(new string[] {
                        fileName,
                        "",
                        "",
                        GetErrorMessage("1.8")
                    });
                }

                foreach (XmlNode node in nodeList) // HOSO
                {
                    ParseFileNodes(node.ChildNodes, fileName);

                    ++Summary.TotalFile;
                }
            }
            catch (Exception ex)
            {
                dtErrorVas.Rows.Add(new string[] {
                    fileName,
                    "",
                    "",
                    GetErrorMessage("1.1")
                });
            }
        }

        private void ParseFileNodes(XmlNodeList fileNodes, string fileName)
        {
            PatientDataContainer patientDataContainer = new PatientDataContainer(fileName);
            string fileType, fileContent;
            foreach (XmlNode fileNode in fileNodes) // FILEHOSO
            {
                try
                {
                    fileType = fileNode.SelectSingleNode("LOAIHOSO").InnerText.Trim().ToUpper();
                    if (fileType != "XML1" && fileType != "XML2" && fileType != "XML3")
                    {
                        throw new Exception();
                    }
                    fileContent = fileNode.SelectSingleNode("NOIDUNGFILE").InnerText.Trim();
                }
                catch
                {
                    dtErrorVas.Rows.Add(new string[] {
                        fileName,
                        "",
                        "",
                        GetErrorMessage("1.11")
                    });
                    continue;
                }
                if (fileContent == "")
                {
                    dtErrorVas.Rows.Add(new string[] {
                        fileName,
                        "",
                        "",
                        GetErrorMessage("1.9")
                    });
                    continue;
                }
                try
                {
                    patientDataContainer.PushFile(
                        fileType,
                        Utils.Encoder.Base64Decode(fileContent)
                    );
                }
                catch
                {
                    dtErrorVas.Rows.Add(new string[] {
                        fileName,
                        "",
                        "",
                        GetErrorMessage("1.10")
                    });
                }
            }

            patientDataContainers.Add(patientDataContainer);

            UpdateSummaryAfterFileAdded(patientDataContainer);

            Validate(patientDataContainers.Count - 1);
        }

        private void UpdateSummaryAfterFileAdded(PatientDataContainer patientDataContainer)
        {
            string MA_LOAI_KCB = patientDataContainer.GetFieldData("XML1.MA_LOAI_KCB")[0];
            if (MA_LOAI_KCB == MA_LOAI_KCB_NGOAI_TRU)
            {
                ++Summary.TotalFileExternal;
            }
            else if (MA_LOAI_KCB == MA_LOAI_KCB_NOI_TRU)
            {
                ++Summary.TotalFileInternal;
            }

            Summary.TotalPillCostXml1 += Util.TryParseInt(patientDataContainer.GetFieldData("XML1.T_THUOC")[0]);
            Summary.TotalEquipCostXml1 += Util.TryParseInt(patientDataContainer.GetFieldData("XML1.T_VTYT")[0]);
            Summary.TotalCost += Util.TryParseInt(patientDataContainer.GetFieldData("XML1.T_TONGCHI")[0]);
            Summary.TotalCostXml1 += Util.TryParseInt(patientDataContainer.GetFieldData("XML1.T_TONGCHI")[0]);
            Summary.TotalCostBhtt += Util.TryParseInt(patientDataContainer.GetFieldData("XML1.T_BHTT")[0]);
            Summary.TotalCostBntt += Util.TryParseInt(patientDataContainer.GetFieldData("XML1.T_BNTT")[0]);
            Summary.TotalCostOther += (Util.TryParseInt(patientDataContainer.GetFieldData("XML1.T_NGUONKHAC")[0]) + Util.TryParseInt(patientDataContainer.GetFieldData("XML1.T_NGOAIDS")[0]));
            Summary.TotalCostDiff += (Summary.TotalCost - (Summary.TotalCostBhtt + Summary.TotalCostBntt + Summary.TotalCostOther));

            string[] pillCosts = patientDataContainer.GetFieldData("XML2.*.THANH_TIEN");
            foreach (string pillCost in pillCosts)
            {
                Summary.TotalPillCostXml2 += Util.TryParseInt(pillCost);
            }
            Summary.TotalPillCostDiff += (Summary.TotalPillCostXml1 - Summary.TotalPillCostXml2);

            string[] equipCosts = patientDataContainer.GetFieldData("XML3.*.THANH_TIEN");
            foreach (string equipCost in equipCosts)
            {
                Summary.TotalEquipCostXml3 += Util.TryParseInt(equipCost);
            }
            Summary.TotalEquipCostDiff += (Summary.TotalEquipCostXml1 - Summary.TotalEquipCostXml3);

            Summary.TotalCostXml23 += (Summary.TotalPillCostXml2 + Summary.TotalEquipCostXml3);
            Summary.TotalCostDiffXml += (Summary.TotalCostXml1 - Summary.TotalCostXml23);
        }

        private void Validate(int patientDataContainerIndex)
        {
            try
            {
                PatientDataContainer patientDataContainer = patientDataContainers[patientDataContainerIndex];
                ValidateState[] states = validator.Check(patientDataContainer);
                if (states.Length > 0)
                {
                    notValidated[patientDataContainerIndex] = states;
                    foreach (ValidateState state in states)
                    {
                        string[] row = new string[] {
                                patientDataContainer.GetFileName(),
                                patientDataContainer.GetFieldData("XML1.HO_TEN")[0],
                                patientDataContainer.GetFieldData("XML1.MA_LK")[0],
                                GetErrorMessage(state.GetRuleOption().RuleError)
                            };
                        switch (state.GetRuleOption().RuleErrorType)
                        {
                            case RuleOption.ERROR_TYPE_VAS:
                                dtErrorVas.Rows.Add(row);
                                break;
                            case RuleOption.ERROR_TYPE_WARNING:
                                dtErrorWarning.Rows.Add(row);
                                break;
                            case RuleOption.ERROR_TYPE_PAID:
                                dtErrorPaid.Rows.Add(row);
                                break;
                        }
                    }

                    ++Summary.TotalFileHasAnyError;
                }
                else
                {
                    ++Summary.TotalFileValidated;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public string[] GetHeader()
        {
            string[] headers = new string[headerIndicators.Count];
            headerIndicators.Values.CopyTo(headers, 0);
            List<string> extraHeaders = new List<string>();
            extraHeaders.Add("Trạng thái hồ sơ");
            extraHeaders.AddRange(headers);
            return extraHeaders.ToArray();
        }

        private bool GoThroughFilterErrorLevel(int filterErrorLevel, PatientDataContainer patientDataContainer, int index, out string status)
        {
            status = "OK";

            bool hasError = notValidated.ContainsKey(index);
            bool hasLevelWarning = false;
            bool hasLevelError = false;
            if (hasError)
            {
                foreach (ValidateState state in notValidated[index])
                {
                    if (state.GetRuleOption().RuleLevel == RuleOption.LEVEL_ERROR)
                    {
                        hasLevelError = true;
                    }
                    else if (state.GetRuleOption().RuleLevel == RuleOption.LEVEL_WARNING)
                    {
                        hasLevelWarning = true;
                    }
                }
            }
            if (filterErrorLevel == RuleOption.LEVEL_NONE && hasError) return false;
            if (filterErrorLevel == RuleOption.LEVEL_ERROR && !hasLevelError) return false;
            if (filterErrorLevel == RuleOption.LEVEL_WARNING && !hasLevelWarning) return false;
            if (filterErrorLevel == RuleOption.LEVEL_ERROR + RuleOption.LEVEL_WARNING && !hasLevelError && !hasLevelWarning) return false;

            if (hasLevelError && hasLevelWarning) status = "Lỗi & Cảnh báo";
            else if (hasLevelError) status = "Lỗi";
            else if (hasLevelWarning) status = "Cảnh báo";

            return true;
        }

        private bool GoThroughFilterPlace(int filterPlace, PatientDataContainer patientDataContainer, int index)
        {
            string MA_LOAI_KCB = patientDataContainer.GetFieldData("XML1.MA_LOAI_KCB")[0];
            if (filterPlace == -1) return true;
            if (filterPlace == 1 && MA_LOAI_KCB == MA_LOAI_KCB_KHAM_BENH) return true; // Kham benh
            if (filterPlace == 2 && MA_LOAI_KCB == MA_LOAI_KCB_NGOAI_TRU) return true; // Ngoai tru
            if (filterPlace == 3 && MA_LOAI_KCB == MA_LOAI_KCB_NOI_TRU) return true; // Noi tru
            if (filterPlace == 4 && (MA_LOAI_KCB == MA_LOAI_KCB_KHAM_BENH || MA_LOAI_KCB == MA_LOAI_KCB_NGOAI_TRU)) return true;

            return false;
        }

        private bool GoThroughFilterSearch(string filterSearch, PatientDataContainer patientDataContainer, int index)
        {
            if (string.IsNullOrEmpty(filterSearch)) return true;

            string HO_TEN = patientDataContainer.GetFieldData("XML1.HO_TEN")[0];
            string MA_BN = patientDataContainer.GetFieldData("XML1.MA_BN")[0];
            if (HO_TEN.ToUpper().Contains(filterSearch.ToUpper())) return true;
            if (MA_BN.ToUpper().Contains(filterSearch.ToUpper())) return true;

            return false;
        }

        public string[][] GetRows(int filterErrorLevel = -1, int filterPlace = -1, string filterSearch = "")
        {
            List<string[]> rows = new List<string[]>();
            int i = 0;
            foreach (PatientDataContainer patientDataContainer in patientDataContainers)
            {
                string status;

                if (!GoThroughFilterErrorLevel(filterErrorLevel, patientDataContainer, i, out status)
                    || !GoThroughFilterPlace(filterPlace, patientDataContainer, i)
                    || !GoThroughFilterSearch(filterSearch, patientDataContainer, i))
                {
                    continue;
                }

                List<string> row = new List<string>();
                row.Add(status);
                foreach (string headerIndicator in headerIndicators.Keys)
                {
                    string[] fieldData = patientDataContainer.GetFieldData(headerIndicator);
                    row.Add(fieldData == null || fieldData.Length == 0 ? "" : fieldData[0]);
                }
                rows.Add(row.ToArray());
                ++i;
            }
            return rows.ToArray();
        }

        public Dictionary<string, string> GetPatientDataXml(int patientDataContainerIndex)
        {
            return patientDataContainers[patientDataContainerIndex].GetXml();
        }

        public string[] GetErrorHeader()
        {
            return new string[]
            {
                "File",
                "Tên bệnh nhân",
                "Mã LK",
                "Nội dung"
            };
        }
    }
}
