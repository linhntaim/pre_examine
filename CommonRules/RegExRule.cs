﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using XmlImport.DataStructure;
using XmlImport.Validation;
using XmlImport.Validation.State;

namespace CommonRules
{
    public class RegExRule : BasicRule, IRule
    {
        public string Name
        {
            get
            {
                return "regex";
            }
        }

        public ValidateState Check(string fieldData, string fieldDataRaw, RuleOption ruleOption, PatientDataContainer patientDataContainer, int index)
        {
            if (!Regex.IsMatch(ruleOption.UseDataRaw ? fieldDataRaw : fieldData, ruleOption.Get("value")))
            {
                return new NotValidated(ruleOption);
            }
            return new Validated();
        }
    }
}
