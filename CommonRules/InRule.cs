﻿using System.Collections.Generic;
using XmlImport.DataStructure;
using XmlImport.Validation;
using XmlImport.Validation.State;

namespace CommonRules
{
    public class InRule : BasicRule, IRule
    {
        public string Name
        {
            get
            {
                return "in";
            }
        }

        public ValidateState Check(string fieldData, string fieldDataRaw, RuleOption ruleOption, PatientDataContainer patientDataContainer, int index)
        {
            List<string> values = new List<string>(ruleOption.Get("value").Split(','));
            if (!values.Contains(fieldData))
            {
                return new NotValidated(ruleOption);
            }
            return new Validated();
        }
    }
}
