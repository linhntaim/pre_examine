﻿using MetaConnect;
using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using XmlImport.DataStructure;
using XmlImport.Validation;
using XmlImport.Validation.State;

namespace CommonRules
{
    public class MetaCompareDecimalRule : BasicRule, IRule
    {
        public string Name
        {
            get
            {
                return "meta_compare_decimal";
            }
        }

        public ValidateState Check(string fieldData, string fieldDataRaw, RuleOption ruleOption, PatientDataContainer patientDataContainer, int index)
        {
            try
            {
                string meta = ruleOption.Get("meta");
                string metaCompareKey = ruleOption.Get("metacomparekey");
                string difference = ruleOption.Get("difference");
                decimal diff = string.IsNullOrEmpty(difference) ? 0 : decimal.Parse(difference);
                string comparator = ruleOption.Get("comparator");
                List<string[]> wheres = new List<string[]>();

                decimal d = decimal.Parse(fieldData);

                metaCompareKey = "CAST(REPLACE(" + metaCompareKey + ",',','') as DECIMAL)";

                string left = d.ToString();
                string right = metaCompareKey;
                switch (comparator)
                {
                    case "eq":
                        comparator = "=";
                        break;
                    case "ne":
                        comparator = "<>";
                        break;
                    case "lt":
                        comparator = "<";
                        break;
                    case "lte":
                        comparator = "<=";
                        break;
                    case "gt":
                        comparator = ">";
                        break;
                    case "gte":
                        comparator = ">=";
                        break;
                    case "dlt":
                        comparator = "<";
                        left = "ABS(" + d.ToString() + "-" + metaCompareKey + ")";
                        right = diff.ToString();
                        break;
                    case "dlte":
                        comparator = "<=";
                        left = "ABS(" + d.ToString() + "-" + metaCompareKey + ")";
                        right = diff.ToString();
                        break;
                    case "dgt":
                        comparator = ">";
                        left = "ABS(" + d.ToString() + "-" + metaCompareKey + ")";
                        right = diff.ToString();
                        break;
                    case "dgte":
                        comparator = ">=";
                        left = "ABS(" + d.ToString() + "-" + metaCompareKey + ")";
                        right = diff.ToString();
                        break;
                }

                wheres.Add(new string[] { left, comparator, right });

                string metaKey = ruleOption.Get("metakey");
                string metaKeyField = ruleOption.Get("metakeyfield");
                if (!string.IsNullOrEmpty(metaKey) && !string.IsNullOrEmpty(metaKeyField))
                {
                    wheres.Add(new string[] { metaKey, WrapString(GetRealFieldData(patientDataContainer.GetFieldData(metaKeyField.Replace("*", index.ToString())))) });
                }
                string metaWhere = ruleOption.Get("metawhere");
                if (!string.IsNullOrEmpty(metaWhere))
                {
                    Queue<string> metaWheres = new Queue<string>(metaWhere.Split(','));
                    while (metaWheres.Count >= 2)
                    {
                        wheres.Add(new string[] { metaWheres.Dequeue(), metaWheres.Dequeue() });
                    }
                }

                Connector connector = new Connector();
                if (!connector.Existed(meta, wheres))
                {
                    return new NotValidated(ruleOption);
                }
            }
            catch
            {
                return new NotValidated(ruleOption);
            }

            return new Validated();
        }

        protected string WrapString(string str)
        {
            return "'" + str + "'";
        }

        protected string WrapFunction(string function, string str)
        {
            return function + "(" + str + ")";
        }
    }
}
