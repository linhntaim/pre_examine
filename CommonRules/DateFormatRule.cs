﻿using System;
using System.Collections.Generic;
using XmlImport.DataStructure;
using XmlImport.Validation;
using XmlImport.Validation.State;

namespace CommonRules
{
    public class DateFormatRule : BasicRule, IRule
    {
        public string Name
        {
            get
            {
                return "date_format";
            }
        }

        public ValidateState Check(string fieldData, string fieldDataRaw, RuleOption ruleOption, PatientDataContainer patientDataContainer, int index)
        {
            try
            {
                DateTime.ParseExact(fieldData, ruleOption.Get("value"), null);
            }
            catch
            {
                return new NotValidated(ruleOption);
            }
            return new Validated();
        }
    }
}
