﻿using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using XmlImport.DataStructure;
using XmlImport.Validation;
using XmlImport.Validation.State;

namespace CommonRules
{
    public class CompareIntRule : BasicRule, IRule
    {
        public string Name
        {
            get
            {
                return "compare_int";
            }
        }

        public ValidateState Check(string fieldData, string fieldDataRaw, RuleOption ruleOption, PatientDataContainer patientDataContainer, int index)
        {
            try
            {
                string extra = ruleOption.Get("extra");
                int e = string.IsNullOrEmpty(extra) ? 0 : int.Parse(extra);
                string difference = ruleOption.Get("difference");
                int diff = string.IsNullOrEmpty(difference) ? 0 : int.Parse(difference);
                string comparator = ruleOption.Get("comparator");
                int d = int.Parse(fieldData);
                if (d.ToString() != fieldData) return new NotValidated(ruleOption);

                string value = ruleOption.Get("value");
                if (!string.IsNullOrEmpty(value))
                {
                    if (Compare(comparator, d, int.Parse(value) + e, diff))
                    {
                        return new Validated();
                    }
                }
                value = ruleOption.Get("sum");
                if (!string.IsNullOrEmpty(value))
                {
                    int sum = 0;
                    string[] fields = value.Split(',');
                    foreach (string field in fields)
                    {
                        string[] sumItems = patientDataContainer.GetFieldData(field);
                        foreach (string sumItem in sumItems)
                        {
                            if (!string.IsNullOrEmpty(sumItem))
                            {
                                sum += int.Parse(sumItem);
                            }
                        }
                    }
                    if (Compare(comparator, d, sum + e, diff))
                    {
                        return new Validated();
                    }
                }
                value = ruleOption.Get("sumh");
                if (!string.IsNullOrEmpty(value))
                {
                    int sum = 0;
                    string[] fields = value.Split(',');
                    foreach (string field in fields)
                    {
                        string[] sumItems = patientDataContainer.GetFieldData(field.Replace("*", index.ToString()));
                        foreach (string sumItem in sumItems)
                        {
                            if (!string.IsNullOrEmpty(sumItem))
                            {
                                sum += int.Parse(sumItem);
                            }
                        }
                    }
                    if (Compare(comparator, d, sum + e, diff))
                    {
                        return new Validated();
                    }
                }
                value = ruleOption.Get("multiply");
                if (!string.IsNullOrEmpty(value))
                {
                    int sum = 1;
                    string[] fields = value.Split(',');
                    foreach (string field in fields)
                    {
                        string[] sumItems = patientDataContainer.GetFieldData(field);
                        foreach (string sumItem in sumItems)
                        {
                            if (!string.IsNullOrEmpty(sumItem))
                            {
                                sum *= int.Parse(sumItem);
                            }
                        }
                    }
                    if (Compare(comparator, d, sum * e, diff))
                    {
                        return new Validated();
                    }
                }
                value = ruleOption.Get("multiplyh");
                if (!string.IsNullOrEmpty(value))
                {
                    int sum = 1;
                    string[] fields = value.Split(',');
                    foreach (string field in fields)
                    {
                        string[] sumItems = patientDataContainer.GetFieldData(field.Replace("*", index.ToString()));
                        foreach (string sumItem in sumItems)
                        {
                            if (!string.IsNullOrEmpty(sumItem))
                            {
                                sum *= int.Parse(sumItem);
                            }
                        }
                    }
                    if (Compare(comparator, d, sum * e, diff))
                    {
                        return new Validated();
                    }
                }
                return new NotValidated(ruleOption);
            }
            catch
            {
                return new NotValidated(ruleOption);
            }
        }

        protected bool Compare(string comparator, int left, int right, int diff)
        {
            switch (comparator)
            {
                case "eq":
                    return left == right;
                case "ne":
                    return left != right;
                case "lt":
                    return left < right;
                case "lte":
                    return left <= right;
                case "gt":
                    return left > right;
                case "gte":
                    return left >= right;
                case "dlt":
                    return Math.Abs(left - right) < diff;
                case "dlte":
                    return Math.Abs(left - right) <= diff;
                case "dgt":
                    return Math.Abs(left - right) > diff;
                case "dgte":
                    return Math.Abs(left - right) >= diff;
            }

            return false;
        }
    }
}
