﻿using MetaConnect;
using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using XmlImport.DataStructure;
using XmlImport.Validation;
using XmlImport.Validation.State;

namespace CommonRules
{
    public class MetaExistedRule : BasicRule, IRule
    {
        public string Name
        {
            get
            {
                return "meta_existed";
            }
        }

        public ValidateState Check(string fieldData, string fieldDataRaw, RuleOption ruleOption, PatientDataContainer patientDataContainer, int index)
        {
            try
            {
                string meta = ruleOption.Get("meta");
                string metaExistedKey = ruleOption.Get("metaexistedkey");
                string metaExistedFunction = ruleOption.Get("metaexistedfunction");
                List<string[]> wheres = new List<string[]>();
                if (!string.IsNullOrEmpty(metaExistedFunction))
                {
                    metaExistedKey = WrapFunction(metaExistedFunction, metaExistedKey);
                    fieldData = WrapFunction(metaExistedFunction, WrapString(fieldData));
                }
                else
                {
                    fieldData = WrapString(fieldData);
                }
                wheres.Add(new string[] { metaExistedKey, fieldData }); ;

                string metaKey = ruleOption.Get("metakey");
                string metaKeyField = ruleOption.Get("metakeyfield");
                if (!string.IsNullOrEmpty(metaKey) && !string.IsNullOrEmpty(metaKeyField))
                {
                    wheres.Add(new string[] { metaKey, WrapString(GetRealFieldData(patientDataContainer.GetFieldData(metaKeyField.Replace("*", index.ToString())))) });
                }
                string metaWhere = ruleOption.Get("metawhere");
                if (!string.IsNullOrEmpty(metaWhere))
                {
                    Queue<string> metaWheres = new Queue<string>(metaWhere.Split(','));
                    while (metaWheres.Count >= 2)
                    {
                        wheres.Add(new string[] { metaWheres.Dequeue(), metaWheres.Dequeue() });
                    }
                }

                Connector connector = new Connector();
                if (!connector.Existed(meta, wheres))
                {
                    return new NotValidated(ruleOption);
                }
            }
            catch
            {
                return new NotValidated(ruleOption);
            }

            return new Validated();
        }

        protected string WrapString(string str)
        {
            return "'" + str + "'";
        }

        protected string WrapFunction(string function, string str)
        {
            return function + "(" + str + ")";
        }
    }
}
