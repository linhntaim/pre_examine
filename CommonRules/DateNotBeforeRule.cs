﻿using System;
using System.Collections.Generic;
using XmlImport.DataStructure;
using XmlImport.Validation;
using XmlImport.Validation.State;

namespace CommonRules
{
    public class DateNotBeforeRule : BasicRule, IRule
    {
        public string Name
        {
            get
            {
                return "date_not_before";
            }
        }

        public ValidateState Check(string fieldData, string fieldDataRaw, RuleOption ruleOption, PatientDataContainer patientDataContainer, int index)
        {
            try
            {
                DateTime currentDate = DateTime.ParseExact(fieldData, ruleOption.Get("format"), null);
                DateTime targetDate = DateTime.Today;
                string targetField = ruleOption.Get("targetfield");
                if (!string.IsNullOrEmpty(targetField))
                {
                    string realTargetFieldData = GetRealFieldData(patientDataContainer.GetFieldData(targetField.Replace("*", index.ToString())));
                    targetDate = DateTime.ParseExact(realTargetFieldData, ruleOption.Get("format"), null);
                }
                if (targetDate > currentDate)
                {
                    return new NotValidated(ruleOption);
                }
            }
            catch
            {
                return new NotValidated(ruleOption);
            }

            return new Validated();
        }
    }
}
