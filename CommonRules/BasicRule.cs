﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CommonRules
{
    public class BasicRule
    {
        protected string GetRealFieldData(string[] fieldData)
        {
            return fieldData == null || fieldData.Length == 0 ? null : fieldData[0];
        }
    }
}
