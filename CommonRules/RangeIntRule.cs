﻿using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using XmlImport.DataStructure;
using XmlImport.Validation;
using XmlImport.Validation.State;

namespace CommonRules
{
    public class RangeIntRule : BasicRule, IRule
    {
        public string Name
        {
            get
            {
                return "range_int";
            }
        }

        public ValidateState Check(string fieldData, string fieldDataRaw, RuleOption ruleOption, PatientDataContainer patientDataContainer, int index)
        {
            try
            {
                int max = int.Parse(ruleOption.Get("max"));
                int min = int.Parse(ruleOption.Get("min"));
                int d = int.Parse(fieldData);
                if (d.ToString() != fieldData) return new NotValidated(ruleOption);
                if (d < min || d > max) return new NotValidated(ruleOption);
            }
            catch
            {
                return new NotValidated(ruleOption);
            }
            return new Validated();
        }
    }
}
