﻿using XmlImport.DataStructure;
using XmlImport.Validation;
using XmlImport.Validation.State;

namespace CommonRules
{
    public class RequiredRule : BasicRule, IRule
    {
        public string Name
        {
            get
            {
                return "required";
            }
        }

        public ValidateState Check(string fieldData, string fieldDataRaw, RuleOption ruleOption, PatientDataContainer patientDataContainer, int index)
        {
            if (fieldData == null || fieldData.Trim() == "") return new NotValidated(ruleOption);
            return new Validated();
        }
    }
}
