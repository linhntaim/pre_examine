﻿using XmlImport.DataStructure;
using XmlImport.Validation;
using XmlImport.Validation.State;

namespace CommonRules
{
    public class NotRequiredRule : BasicRule, IRule
    {
        public string Name
        {
            get
            {
                return "sometimes";
            }
        }

        public ValidateState Check(string fieldData, string fieldDataRaw, RuleOption ruleOption, PatientDataContainer patientDataContainer, int index)
        {
            if (fieldData == null || fieldData.Trim() == "")
            {
                return new BlockValidated();
            }
            return new Validated();
        }
    }
}
