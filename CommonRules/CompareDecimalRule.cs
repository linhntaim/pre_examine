﻿using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using XmlImport.DataStructure;
using XmlImport.Validation;
using XmlImport.Validation.State;

namespace CommonRules
{
    public class CompareDecimalRule : BasicRule, IRule
    {
        public string Name
        {
            get
            {
                return "compare_decimal";
            }
        }

        public ValidateState Check(string fieldData, string fieldDataRaw, RuleOption ruleOption, PatientDataContainer patientDataContainer, int index)
        {
            try
            {
                string extra = ruleOption.Get("extra");
                decimal e = string.IsNullOrEmpty(extra) ? 0 : decimal.Parse(extra);
                string difference = ruleOption.Get("difference");
                decimal diff = string.IsNullOrEmpty(difference) ? 0 : decimal.Parse(difference);
                string comparator = ruleOption.Get("comparator");
                decimal d = decimal.Parse(fieldData);
                if (d.ToString() != fieldData) return new NotValidated(ruleOption);

                string value = ruleOption.Get("value");
                if (!string.IsNullOrEmpty(value))
                {
                    if (Compare(comparator, d, decimal.Parse(value) + e, diff))
                    {
                        return new Validated();
                    }
                }
                value = ruleOption.Get("sum");
                if (!string.IsNullOrEmpty(value))
                {
                    decimal sum = 0;
                    string[] fields = value.Split(',');
                    foreach (string field in fields)
                    {
                        string[] sumItems = patientDataContainer.GetFieldData(field);
                        foreach (string sumItem in sumItems)
                        {
                            if (!string.IsNullOrEmpty(sumItem))
                            {
                                sum += decimal.Parse(sumItem);
                            }
                        }
                    }
                    if (Compare(comparator, d, sum + e, diff))
                    {
                        return new Validated();
                    }
                }
                value = ruleOption.Get("sumh");
                if (!string.IsNullOrEmpty(value))
                {
                    decimal sum = 0;
                    string[] fields = value.Split(',');
                    foreach (string field in fields)
                    {
                        string[] sumItems = patientDataContainer.GetFieldData(field.Replace("*", index.ToString()));
                        foreach (string sumItem in sumItems)
                        {
                            if (!string.IsNullOrEmpty(sumItem))
                            {
                                sum += decimal.Parse(sumItem);
                            }
                        }
                    }
                    if (Compare(comparator, d, sum + e, diff))
                    {
                        return new Validated();
                    }
                }
                value = ruleOption.Get("multiply");
                if (!string.IsNullOrEmpty(value))
                {
                    decimal sum = 1;
                    string[] fields = value.Split(',');
                    foreach (string field in fields)
                    {
                        string[] sumItems = patientDataContainer.GetFieldData(field);
                        foreach (string sumItem in sumItems)
                        {
                            if (!string.IsNullOrEmpty(sumItem))
                            {
                                sum *= decimal.Parse(sumItem);
                            }
                        }
                    }
                    if (Compare(comparator, d, sum * e, diff))
                    {
                        return new Validated();
                    }
                }
                value = ruleOption.Get("multiplyh");
                if (!string.IsNullOrEmpty(value))
                {
                    decimal sum = 1;
                    string[] fields = value.Split(',');
                    foreach (string field in fields)
                    {
                        string[] sumItems = patientDataContainer.GetFieldData(field.Replace("*", index.ToString()));
                        foreach (string sumItem in sumItems)
                        {
                            if (!string.IsNullOrEmpty(sumItem))
                            {
                                sum *= decimal.Parse(sumItem);
                            }
                        }
                    }
                    if (Compare(comparator, d, sum * e, diff))
                    {
                        return new Validated();
                    }
                }
                return new NotValidated(ruleOption);
            }
            catch
            {
                return new NotValidated(ruleOption);
            }
        }

        protected bool Compare(string comparator, decimal left, decimal right, decimal diff)
        {
            switch (comparator)
            {
                case "eq":
                    return left == right;
                case "ne":
                    return left != right;
                case "lt":
                    return left < right;
                case "lte":
                    return left <= right;
                case "gt":
                    return left > right;
                case "gte":
                    return left >= right;
                case "dlt":
                    return Math.Abs(left - right) < diff;
                case "dlte":
                    return Math.Abs(left - right) <= diff;
                case "dgt":
                    return Math.Abs(left - right) > diff;
                case "dgte":
                    return Math.Abs(left - right) >= diff;
            }

            return false;
        }
    }
}
