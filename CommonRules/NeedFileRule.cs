﻿using System;
using System.Collections.Generic;
using XmlImport.DataStructure;
using XmlImport.Validation;
using XmlImport.Validation.State;

namespace CommonRules
{
    public class NeedFileRule : BasicRule, IRule
    {
        public string Name
        {
            get
            {
                return "need_file";
            }
        }

        public ValidateState Check(string fieldData, string fieldDataRaw, RuleOption ruleOption, PatientDataContainer patientDataContainer, int index)
        {
            if (!patientDataContainer.HasFile(ruleOption.Get("value")))
            {
                return new NotValidated(ruleOption);
            }

            return new Validated();
        }
    }
}
