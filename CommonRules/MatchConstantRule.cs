﻿using System.Collections.Generic;
using XmlImport.DataStructure;
using XmlImport.Validation;
using XmlImport.Validation.State;

namespace CommonRules
{
    public class MatchConstantRule : BasicRule, IRule
    {
        public string Name
        {
            get
            {
                return "match_constant";
            }
        }

        public ValidateState Check(string fieldData, string fieldDataRaw, RuleOption ruleOption, PatientDataContainer patientDataContainer, int index)
        {
            if (fieldData != ruleOption.Constants[ruleOption.Get("value")])
            {
                return new NotValidated(ruleOption);
            }
            return new Validated();
        }
    }
}
