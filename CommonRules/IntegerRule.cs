﻿using System.Collections.Generic;
using XmlImport.DataStructure;
using XmlImport.Validation;
using XmlImport.Validation.State;

namespace CommonRules
{
    public class IntegerRule : BasicRule, IRule
    {
        public string Name
        {
            get
            {
                return "integer";
            }
        }

        public ValidateState Check(string fieldData, string fieldDataRaw, RuleOption ruleOption, PatientDataContainer patientDataContainer, int index)
        {
            try
            {
                if (int.Parse(fieldData).ToString() != fieldData)
                {
                    return new NotValidated(ruleOption);
                }
            }
            catch
            {
                return new NotValidated(ruleOption);
            }
            return new Validated();
        }
    }
}
