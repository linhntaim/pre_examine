﻿using XmlImport.DataStructure;
using XmlImport.Validation;
using XmlImport.Validation.State;

namespace CommonRules
{
    public class MaxCharRule : BasicRule, IRule
    {
        public string Name
        {
            get
            {
                return "max_char";
            }
        }

        public ValidateState Check(string fieldData, string fieldDataRaw, RuleOption ruleOption, PatientDataContainer patientDataContainer, int index)
        {
            int max = int.Parse(ruleOption.Get("value"));
            if (fieldData.Length > max)
            {
                return new NotValidated(ruleOption);
            }
            return new Validated();
        }
    }
}
