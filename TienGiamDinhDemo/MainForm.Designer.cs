﻿namespace TienGiamDinhDemo
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnImportXml = new System.Windows.Forms.Button();
            this.grvPatients = new System.Windows.Forms.DataGridView();
            this.bgwReading = new System.ComponentModel.BackgroundWorker();
            this.menMain = new System.Windows.Forms.MenuStrip();
            this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.settingsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.importMetaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.importConfigurationToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.btnViewXml = new System.Windows.Forms.Button();
            this.rbnFilterAll = new System.Windows.Forms.RadioButton();
            this.rbnFilterAnyError = new System.Windows.Forms.RadioButton();
            this.rbnFilterOk = new System.Windows.Forms.RadioButton();
            this.rbnFilterError = new System.Windows.Forms.RadioButton();
            this.rbnFilterWarning = new System.Windows.Forms.RadioButton();
            this.panel1 = new System.Windows.Forms.Panel();
            this.panel2 = new System.Windows.Forms.Panel();
            this.rbnAnyPlace = new System.Windows.Forms.RadioButton();
            this.rbnFilterExtenal = new System.Windows.Forms.RadioButton();
            this.rbnFilterAnyExternal = new System.Windows.Forms.RadioButton();
            this.rbnFilterInner = new System.Windows.Forms.RadioButton();
            this.rbnFilterClinic = new System.Windows.Forms.RadioButton();
            this.tclError = new System.Windows.Forms.TabControl();
            this.tabVas = new System.Windows.Forms.TabPage();
            this.grvVas = new System.Windows.Forms.DataGridView();
            this.tabWarning = new System.Windows.Forms.TabPage();
            this.grvWarning = new System.Windows.Forms.DataGridView();
            this.tabPaid = new System.Windows.Forms.TabPage();
            this.grvPaid = new System.Windows.Forms.DataGridView();
            this.panel6 = new System.Windows.Forms.Panel();
            this.lblTotalEquipCostDiff = new System.Windows.Forms.Label();
            this.lblTotalEquipCostXml2 = new System.Windows.Forms.Label();
            this.lblTotalEquipCostXml1 = new System.Windows.Forms.Label();
            this.panel3 = new System.Windows.Forms.Panel();
            this.lblTotalFileExternal = new System.Windows.Forms.Label();
            this.lblTotalFileInternal = new System.Windows.Forms.Label();
            this.lblTotalFileHasAnyError = new System.Windows.Forms.Label();
            this.lblTotalFileValidated = new System.Windows.Forms.Label();
            this.lblTotalFile = new System.Windows.Forms.Label();
            this.panel4 = new System.Windows.Forms.Panel();
            this.lblTotalCostDiff = new System.Windows.Forms.Label();
            this.lblTotalCostOther = new System.Windows.Forms.Label();
            this.lblTotalCostBntt = new System.Windows.Forms.Label();
            this.lblTotalCostBhtt = new System.Windows.Forms.Label();
            this.lblTotalCost = new System.Windows.Forms.Label();
            this.panel5 = new System.Windows.Forms.Panel();
            this.lblTotalPillCostXml2 = new System.Windows.Forms.Label();
            this.lblTotalPillCostXml1 = new System.Windows.Forms.Label();
            this.lblTotalPillCostDiff = new System.Windows.Forms.Label();
            this.lblTotalCostDiffXml = new System.Windows.Forms.Label();
            this.lblTotalCostXml23 = new System.Windows.Forms.Label();
            this.lblTotalCostXml1 = new System.Windows.Forms.Label();
            this.pnlFooter = new System.Windows.Forms.Panel();
            this.lblTime = new System.Windows.Forms.Label();
            this.lblName = new System.Windows.Forms.Label();
            this.pnlSummary = new System.Windows.Forms.Panel();
            this.tbxSearch = new System.Windows.Forms.TextBox();
            this.btnSearch = new System.Windows.Forms.Button();
            this.btnExport = new System.Windows.Forms.Button();
            this.btnExportVas = new System.Windows.Forms.Button();
            this.btnExportWarning = new System.Windows.Forms.Button();
            this.btnExportPaid = new System.Windows.Forms.Button();
            this.btnClearSearch = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.grvPatients)).BeginInit();
            this.menMain.SuspendLayout();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.tclError.SuspendLayout();
            this.tabVas.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grvVas)).BeginInit();
            this.tabWarning.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grvWarning)).BeginInit();
            this.tabPaid.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grvPaid)).BeginInit();
            this.panel6.SuspendLayout();
            this.panel3.SuspendLayout();
            this.panel4.SuspendLayout();
            this.panel5.SuspendLayout();
            this.pnlFooter.SuspendLayout();
            this.pnlSummary.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnImportXml
            // 
            this.btnImportXml.Location = new System.Drawing.Point(12, 32);
            this.btnImportXml.Name = "btnImportXml";
            this.btnImportXml.Size = new System.Drawing.Size(75, 23);
            this.btnImportXml.TabIndex = 2;
            this.btnImportXml.Text = "Nhập XML";
            this.btnImportXml.UseVisualStyleBackColor = true;
            this.btnImportXml.Click += new System.EventHandler(this.btnImportXml_Click);
            // 
            // grvPatients
            // 
            this.grvPatients.AllowUserToAddRows = false;
            this.grvPatients.AllowUserToDeleteRows = false;
            this.grvPatients.AllowUserToResizeRows = false;
            this.grvPatients.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.grvPatients.Location = new System.Drawing.Point(12, 138);
            this.grvPatients.MultiSelect = false;
            this.grvPatients.Name = "grvPatients";
            this.grvPatients.ReadOnly = true;
            this.grvPatients.Size = new System.Drawing.Size(547, 318);
            this.grvPatients.TabIndex = 20;
            // 
            // bgwReading
            // 
            this.bgwReading.WorkerReportsProgress = true;
            this.bgwReading.WorkerSupportsCancellation = true;
            this.bgwReading.DoWork += new System.ComponentModel.DoWorkEventHandler(this.bgwReading_DoWork);
            this.bgwReading.ProgressChanged += new System.ComponentModel.ProgressChangedEventHandler(this.bgwReading_ProgressChanged);
            this.bgwReading.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.bgwReading_RunWorkerCompleted);
            // 
            // menMain
            // 
            this.menMain.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem});
            this.menMain.Location = new System.Drawing.Point(0, 0);
            this.menMain.Name = "menMain";
            this.menMain.Size = new System.Drawing.Size(924, 24);
            this.menMain.TabIndex = 1;
            this.menMain.Text = "Trình đơn chính";
            // 
            // fileToolStripMenuItem
            // 
            this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.settingsToolStripMenuItem,
            this.importMetaToolStripMenuItem,
            this.importConfigurationToolStripMenuItem});
            this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            this.fileToolStripMenuItem.Size = new System.Drawing.Size(37, 20);
            this.fileToolStripMenuItem.Text = "File";
            // 
            // settingsToolStripMenuItem
            // 
            this.settingsToolStripMenuItem.Name = "settingsToolStripMenuItem";
            this.settingsToolStripMenuItem.Size = new System.Drawing.Size(205, 22);
            this.settingsToolStripMenuItem.Text = "Thiết lập";
            this.settingsToolStripMenuItem.Click += new System.EventHandler(this.settingsToolStripMenuItem_Click);
            // 
            // importMetaToolStripMenuItem
            // 
            this.importMetaToolStripMenuItem.Name = "importMetaToolStripMenuItem";
            this.importMetaToolStripMenuItem.Size = new System.Drawing.Size(205, 22);
            this.importMetaToolStripMenuItem.Text = "Danh mục";
            this.importMetaToolStripMenuItem.Click += new System.EventHandler(this.importMetaToolStripMenuItem_Click);
            // 
            // importConfigurationToolStripMenuItem
            // 
            this.importConfigurationToolStripMenuItem.Name = "importConfigurationToolStripMenuItem";
            this.importConfigurationToolStripMenuItem.Size = new System.Drawing.Size(205, 22);
            this.importConfigurationToolStripMenuItem.Text = "Nhập tập tin cấu hình lỗi";
            this.importConfigurationToolStripMenuItem.Click += new System.EventHandler(this.importConfigurationToolStripMenuItem_Click);
            // 
            // btnViewXml
            // 
            this.btnViewXml.Location = new System.Drawing.Point(93, 32);
            this.btnViewXml.Name = "btnViewXml";
            this.btnViewXml.Size = new System.Drawing.Size(124, 23);
            this.btnViewXml.TabIndex = 3;
            this.btnViewXml.Text = "Xem dữ liệu gốc XML";
            this.btnViewXml.UseVisualStyleBackColor = true;
            this.btnViewXml.Click += new System.EventHandler(this.btnViewXml_Click);
            // 
            // rbnFilterAll
            // 
            this.rbnFilterAll.AutoSize = true;
            this.rbnFilterAll.Checked = true;
            this.rbnFilterAll.Location = new System.Drawing.Point(0, 0);
            this.rbnFilterAll.Name = "rbnFilterAll";
            this.rbnFilterAll.Size = new System.Drawing.Size(56, 17);
            this.rbnFilterAll.TabIndex = 5;
            this.rbnFilterAll.TabStop = true;
            this.rbnFilterAll.Text = "Tất cả";
            this.rbnFilterAll.UseVisualStyleBackColor = true;
            this.rbnFilterAll.CheckedChanged += new System.EventHandler(this.rbnFilterAll_CheckedChanged);
            // 
            // rbnFilterAnyError
            // 
            this.rbnFilterAnyError.AutoSize = true;
            this.rbnFilterAnyError.Location = new System.Drawing.Point(153, 0);
            this.rbnFilterAnyError.Name = "rbnFilterAnyError";
            this.rbnFilterAnyError.Size = new System.Drawing.Size(129, 17);
            this.rbnFilterAnyError.TabIndex = 7;
            this.rbnFilterAnyError.Text = "Hồ sơ lỗi và cảnh báo";
            this.rbnFilterAnyError.UseVisualStyleBackColor = true;
            this.rbnFilterAnyError.CheckedChanged += new System.EventHandler(this.rbnFilterAnyError_CheckedChanged);
            // 
            // rbnFilterOk
            // 
            this.rbnFilterOk.AutoSize = true;
            this.rbnFilterOk.Location = new System.Drawing.Point(62, 0);
            this.rbnFilterOk.Name = "rbnFilterOk";
            this.rbnFilterOk.Size = new System.Drawing.Size(85, 17);
            this.rbnFilterOk.TabIndex = 6;
            this.rbnFilterOk.Text = "Hồ sơ hợp lệ";
            this.rbnFilterOk.UseVisualStyleBackColor = true;
            this.rbnFilterOk.CheckedChanged += new System.EventHandler(this.rbnFilterOk_CheckedChanged);
            // 
            // rbnFilterError
            // 
            this.rbnFilterError.AutoSize = true;
            this.rbnFilterError.Location = new System.Drawing.Point(288, 0);
            this.rbnFilterError.Name = "rbnFilterError";
            this.rbnFilterError.Size = new System.Drawing.Size(66, 17);
            this.rbnFilterError.TabIndex = 8;
            this.rbnFilterError.Text = "Hồ sơ lỗi";
            this.rbnFilterError.UseVisualStyleBackColor = true;
            this.rbnFilterError.CheckedChanged += new System.EventHandler(this.rbnFilterError_CheckedChanged);
            // 
            // rbnFilterWarning
            // 
            this.rbnFilterWarning.AutoSize = true;
            this.rbnFilterWarning.Location = new System.Drawing.Point(360, 0);
            this.rbnFilterWarning.Name = "rbnFilterWarning";
            this.rbnFilterWarning.Size = new System.Drawing.Size(101, 17);
            this.rbnFilterWarning.TabIndex = 9;
            this.rbnFilterWarning.Text = "Hồ sơ cảnh báo";
            this.rbnFilterWarning.UseVisualStyleBackColor = true;
            this.rbnFilterWarning.CheckedChanged += new System.EventHandler(this.rbnFilterWarning_CheckedChanged);
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.rbnFilterAll);
            this.panel1.Controls.Add(this.rbnFilterWarning);
            this.panel1.Controls.Add(this.rbnFilterAnyError);
            this.panel1.Controls.Add(this.rbnFilterOk);
            this.panel1.Controls.Add(this.rbnFilterError);
            this.panel1.Location = new System.Drawing.Point(12, 61);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(547, 22);
            this.panel1.TabIndex = 4;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.rbnAnyPlace);
            this.panel2.Controls.Add(this.rbnFilterExtenal);
            this.panel2.Controls.Add(this.rbnFilterAnyExternal);
            this.panel2.Controls.Add(this.rbnFilterInner);
            this.panel2.Controls.Add(this.rbnFilterClinic);
            this.panel2.Location = new System.Drawing.Point(12, 84);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(547, 22);
            this.panel2.TabIndex = 10;
            // 
            // rbnAnyPlace
            // 
            this.rbnAnyPlace.AutoSize = true;
            this.rbnAnyPlace.Checked = true;
            this.rbnAnyPlace.Location = new System.Drawing.Point(0, 0);
            this.rbnAnyPlace.Name = "rbnAnyPlace";
            this.rbnAnyPlace.Size = new System.Drawing.Size(56, 17);
            this.rbnAnyPlace.TabIndex = 11;
            this.rbnAnyPlace.TabStop = true;
            this.rbnAnyPlace.Text = "Tất cả";
            this.rbnAnyPlace.UseVisualStyleBackColor = true;
            this.rbnAnyPlace.CheckedChanged += new System.EventHandler(this.rbnAnyPlace_CheckedChanged);
            // 
            // rbnFilterExtenal
            // 
            this.rbnFilterExtenal.AutoSize = true;
            this.rbnFilterExtenal.Location = new System.Drawing.Point(383, 0);
            this.rbnFilterExtenal.Name = "rbnFilterExtenal";
            this.rbnFilterExtenal.Size = new System.Drawing.Size(102, 17);
            this.rbnFilterExtenal.TabIndex = 15;
            this.rbnFilterExtenal.Text = "Điều trị ngoại trú";
            this.rbnFilterExtenal.UseVisualStyleBackColor = true;
            this.rbnFilterExtenal.CheckedChanged += new System.EventHandler(this.rbnFilterExtenal_CheckedChanged);
            // 
            // rbnFilterAnyExternal
            // 
            this.rbnFilterAnyExternal.AutoSize = true;
            this.rbnFilterAnyExternal.Location = new System.Drawing.Point(124, 0);
            this.rbnFilterAnyExternal.Name = "rbnFilterAnyExternal";
            this.rbnFilterAnyExternal.Size = new System.Drawing.Size(162, 17);
            this.rbnFilterAnyExternal.TabIndex = 13;
            this.rbnFilterAnyExternal.Text = "Phòng khám và ĐT ngoại trú";
            this.rbnFilterAnyExternal.UseVisualStyleBackColor = true;
            this.rbnFilterAnyExternal.CheckedChanged += new System.EventHandler(this.rbnFilterAnyExternal_CheckedChanged);
            // 
            // rbnFilterInner
            // 
            this.rbnFilterInner.AutoSize = true;
            this.rbnFilterInner.Location = new System.Drawing.Point(62, 0);
            this.rbnFilterInner.Name = "rbnFilterInner";
            this.rbnFilterInner.Size = new System.Drawing.Size(56, 17);
            this.rbnFilterInner.TabIndex = 12;
            this.rbnFilterInner.Text = "Nội trú";
            this.rbnFilterInner.UseVisualStyleBackColor = true;
            this.rbnFilterInner.CheckedChanged += new System.EventHandler(this.rbnFilterInner_CheckedChanged);
            // 
            // rbnFilterClinic
            // 
            this.rbnFilterClinic.AutoSize = true;
            this.rbnFilterClinic.Location = new System.Drawing.Point(292, 0);
            this.rbnFilterClinic.Name = "rbnFilterClinic";
            this.rbnFilterClinic.Size = new System.Drawing.Size(85, 17);
            this.rbnFilterClinic.TabIndex = 14;
            this.rbnFilterClinic.Text = "Phòng khám";
            this.rbnFilterClinic.UseVisualStyleBackColor = true;
            this.rbnFilterClinic.CheckedChanged += new System.EventHandler(this.rbnFilterClinic_CheckedChanged);
            // 
            // tclError
            // 
            this.tclError.Controls.Add(this.tabVas);
            this.tclError.Controls.Add(this.tabWarning);
            this.tclError.Controls.Add(this.tabPaid);
            this.tclError.Location = new System.Drawing.Point(565, 75);
            this.tclError.Name = "tclError";
            this.tclError.SelectedIndex = 0;
            this.tclError.Size = new System.Drawing.Size(348, 382);
            this.tclError.TabIndex = 21;
            // 
            // tabVas
            // 
            this.tabVas.Controls.Add(this.btnExportVas);
            this.tabVas.Controls.Add(this.grvVas);
            this.tabVas.Location = new System.Drawing.Point(4, 22);
            this.tabVas.Name = "tabVas";
            this.tabVas.Padding = new System.Windows.Forms.Padding(3);
            this.tabVas.Size = new System.Drawing.Size(340, 356);
            this.tabVas.TabIndex = 0;
            this.tabVas.Text = "Lỗi VAS";
            this.tabVas.UseVisualStyleBackColor = true;
            // 
            // grvVas
            // 
            this.grvVas.AllowUserToAddRows = false;
            this.grvVas.AllowUserToDeleteRows = false;
            this.grvVas.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.grvVas.Location = new System.Drawing.Point(6, 41);
            this.grvVas.Name = "grvVas";
            this.grvVas.ReadOnly = true;
            this.grvVas.Size = new System.Drawing.Size(328, 309);
            this.grvVas.TabIndex = 23;
            // 
            // tabWarning
            // 
            this.tabWarning.Controls.Add(this.btnExportWarning);
            this.tabWarning.Controls.Add(this.grvWarning);
            this.tabWarning.Location = new System.Drawing.Point(4, 22);
            this.tabWarning.Name = "tabWarning";
            this.tabWarning.Padding = new System.Windows.Forms.Padding(3);
            this.tabWarning.Size = new System.Drawing.Size(340, 356);
            this.tabWarning.TabIndex = 1;
            this.tabWarning.Text = "Lỗi cảnh báo";
            this.tabWarning.UseVisualStyleBackColor = true;
            // 
            // grvWarning
            // 
            this.grvWarning.AllowUserToAddRows = false;
            this.grvWarning.AllowUserToDeleteRows = false;
            this.grvWarning.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.grvWarning.Location = new System.Drawing.Point(6, 41);
            this.grvWarning.Name = "grvWarning";
            this.grvWarning.ReadOnly = true;
            this.grvWarning.Size = new System.Drawing.Size(328, 309);
            this.grvWarning.TabIndex = 25;
            // 
            // tabPaid
            // 
            this.tabPaid.Controls.Add(this.btnExportPaid);
            this.tabPaid.Controls.Add(this.grvPaid);
            this.tabPaid.Location = new System.Drawing.Point(4, 22);
            this.tabPaid.Name = "tabPaid";
            this.tabPaid.Padding = new System.Windows.Forms.Padding(3);
            this.tabPaid.Size = new System.Drawing.Size(340, 356);
            this.tabPaid.TabIndex = 2;
            this.tabPaid.Text = "Lỗi xuất toán";
            this.tabPaid.UseVisualStyleBackColor = true;
            // 
            // grvPaid
            // 
            this.grvPaid.AllowUserToAddRows = false;
            this.grvPaid.AllowUserToDeleteRows = false;
            this.grvPaid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.grvPaid.Location = new System.Drawing.Point(6, 41);
            this.grvPaid.Name = "grvPaid";
            this.grvPaid.ReadOnly = true;
            this.grvPaid.Size = new System.Drawing.Size(328, 309);
            this.grvPaid.TabIndex = 27;
            // 
            // panel6
            // 
            this.panel6.Controls.Add(this.lblTotalEquipCostDiff);
            this.panel6.Controls.Add(this.lblTotalEquipCostXml2);
            this.panel6.Controls.Add(this.lblTotalEquipCostXml1);
            this.panel6.Location = new System.Drawing.Point(618, 0);
            this.panel6.Name = "panel6";
            this.panel6.Size = new System.Drawing.Size(200, 102);
            this.panel6.TabIndex = 54;
            // 
            // lblTotalEquipCostDiff
            // 
            this.lblTotalEquipCostDiff.AutoSize = true;
            this.lblTotalEquipCostDiff.Location = new System.Drawing.Point(-2, 32);
            this.lblTotalEquipCostDiff.Name = "lblTotalEquipCostDiff";
            this.lblTotalEquipCostDiff.Size = new System.Drawing.Size(64, 13);
            this.lblTotalEquipCostDiff.TabIndex = 0;
            this.lblTotalEquipCostDiff.Text = "Chênh lệch:";
            // 
            // lblTotalEquipCostXml2
            // 
            this.lblTotalEquipCostXml2.AutoSize = true;
            this.lblTotalEquipCostXml2.Location = new System.Drawing.Point(-3, 16);
            this.lblTotalEquipCostXml2.Name = "lblTotalEquipCostXml2";
            this.lblTotalEquipCostXml2.Size = new System.Drawing.Size(116, 13);
            this.lblTotalEquipCostXml2.TabIndex = 0;
            this.lblTotalEquipCostXml2.Text = "Tổng tiền vật tư XML3:";
            // 
            // lblTotalEquipCostXml1
            // 
            this.lblTotalEquipCostXml1.AutoSize = true;
            this.lblTotalEquipCostXml1.Location = new System.Drawing.Point(-3, 0);
            this.lblTotalEquipCostXml1.Name = "lblTotalEquipCostXml1";
            this.lblTotalEquipCostXml1.Size = new System.Drawing.Size(116, 13);
            this.lblTotalEquipCostXml1.TabIndex = 0;
            this.lblTotalEquipCostXml1.Text = "Tổng tiền vật tư XML1:";
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.lblTotalFileExternal);
            this.panel3.Controls.Add(this.lblTotalFileInternal);
            this.panel3.Controls.Add(this.lblTotalFileHasAnyError);
            this.panel3.Controls.Add(this.lblTotalFileValidated);
            this.panel3.Controls.Add(this.lblTotalFile);
            this.panel3.Location = new System.Drawing.Point(0, 0);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(200, 86);
            this.panel3.TabIndex = 51;
            // 
            // lblTotalFileExternal
            // 
            this.lblTotalFileExternal.AutoSize = true;
            this.lblTotalFileExternal.Location = new System.Drawing.Point(-3, 64);
            this.lblTotalFileExternal.Name = "lblTotalFileExternal";
            this.lblTotalFileExternal.Size = new System.Drawing.Size(122, 13);
            this.lblTotalFileExternal.TabIndex = 0;
            this.lblTotalFileExternal.Text = "Tổng số hồ sơ ngoại trú:";
            // 
            // lblTotalFileInternal
            // 
            this.lblTotalFileInternal.AutoSize = true;
            this.lblTotalFileInternal.Location = new System.Drawing.Point(-3, 48);
            this.lblTotalFileInternal.Name = "lblTotalFileInternal";
            this.lblTotalFileInternal.Size = new System.Drawing.Size(110, 13);
            this.lblTotalFileInternal.TabIndex = 0;
            this.lblTotalFileInternal.Text = "Tổng số hồ sơ nội trú:";
            // 
            // lblTotalFileHasAnyError
            // 
            this.lblTotalFileHasAnyError.AutoSize = true;
            this.lblTotalFileHasAnyError.Location = new System.Drawing.Point(-3, 32);
            this.lblTotalFileHasAnyError.Name = "lblTotalFileHasAnyError";
            this.lblTotalFileHasAnyError.Size = new System.Drawing.Size(141, 13);
            this.lblTotalFileHasAnyError.TabIndex = 0;
            this.lblTotalFileHasAnyError.Text = "Tổng số hồ sơ lỗi/cảnh bảo:";
            // 
            // lblTotalFileValidated
            // 
            this.lblTotalFileValidated.AutoSize = true;
            this.lblTotalFileValidated.Location = new System.Drawing.Point(-3, 16);
            this.lblTotalFileValidated.Name = "lblTotalFileValidated";
            this.lblTotalFileValidated.Size = new System.Drawing.Size(113, 13);
            this.lblTotalFileValidated.TabIndex = 0;
            this.lblTotalFileValidated.Text = "Tổng số hồ sơ hợp lệ: ";
            // 
            // lblTotalFile
            // 
            this.lblTotalFile.AutoSize = true;
            this.lblTotalFile.Location = new System.Drawing.Point(-3, 0);
            this.lblTotalFile.Name = "lblTotalFile";
            this.lblTotalFile.Size = new System.Drawing.Size(81, 13);
            this.lblTotalFile.TabIndex = 0;
            this.lblTotalFile.Text = "Tổng số hồ sơ: ";
            // 
            // panel4
            // 
            this.panel4.Controls.Add(this.lblTotalCostDiff);
            this.panel4.Controls.Add(this.lblTotalCostOther);
            this.panel4.Controls.Add(this.lblTotalCostBntt);
            this.panel4.Controls.Add(this.lblTotalCostBhtt);
            this.panel4.Controls.Add(this.lblTotalCost);
            this.panel4.Location = new System.Drawing.Point(206, 0);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(200, 86);
            this.panel4.TabIndex = 52;
            // 
            // lblTotalCostDiff
            // 
            this.lblTotalCostDiff.AutoSize = true;
            this.lblTotalCostDiff.Location = new System.Drawing.Point(-2, 64);
            this.lblTotalCostDiff.Name = "lblTotalCostDiff";
            this.lblTotalCostDiff.Size = new System.Drawing.Size(64, 13);
            this.lblTotalCostDiff.TabIndex = 0;
            this.lblTotalCostDiff.Text = "Chênh lệch:";
            // 
            // lblTotalCostOther
            // 
            this.lblTotalCostOther.AutoSize = true;
            this.lblTotalCostOther.Location = new System.Drawing.Point(-3, 48);
            this.lblTotalCostOther.Name = "lblTotalCostOther";
            this.lblTotalCostOther.Size = new System.Drawing.Size(62, 13);
            this.lblTotalCostOther.TabIndex = 0;
            this.lblTotalCostOther.Text = "Tổng khác:";
            // 
            // lblTotalCostBntt
            // 
            this.lblTotalCostBntt.AutoSize = true;
            this.lblTotalCostBntt.Location = new System.Drawing.Point(-3, 32);
            this.lblTotalCostBntt.Name = "lblTotalCostBntt";
            this.lblTotalCostBntt.Size = new System.Drawing.Size(67, 13);
            this.lblTotalCostBntt.TabIndex = 0;
            this.lblTotalCostBntt.Text = "Tổng BNTT:";
            // 
            // lblTotalCostBhtt
            // 
            this.lblTotalCostBhtt.AutoSize = true;
            this.lblTotalCostBhtt.Location = new System.Drawing.Point(-3, 16);
            this.lblTotalCostBhtt.Name = "lblTotalCostBhtt";
            this.lblTotalCostBhtt.Size = new System.Drawing.Size(67, 13);
            this.lblTotalCostBhtt.TabIndex = 0;
            this.lblTotalCostBhtt.Text = "Tổng BHTT:";
            // 
            // lblTotalCost
            // 
            this.lblTotalCost.AutoSize = true;
            this.lblTotalCost.Location = new System.Drawing.Point(-3, 0);
            this.lblTotalCost.Name = "lblTotalCost";
            this.lblTotalCost.Size = new System.Drawing.Size(71, 13);
            this.lblTotalCost.TabIndex = 0;
            this.lblTotalCost.Text = "Tổng chi phí:";
            // 
            // panel5
            // 
            this.panel5.Controls.Add(this.lblTotalPillCostXml2);
            this.panel5.Controls.Add(this.lblTotalPillCostXml1);
            this.panel5.Controls.Add(this.lblTotalPillCostDiff);
            this.panel5.Controls.Add(this.lblTotalCostDiffXml);
            this.panel5.Controls.Add(this.lblTotalCostXml23);
            this.panel5.Controls.Add(this.lblTotalCostXml1);
            this.panel5.Location = new System.Drawing.Point(412, 0);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(200, 102);
            this.panel5.TabIndex = 53;
            // 
            // lblTotalPillCostXml2
            // 
            this.lblTotalPillCostXml2.AutoSize = true;
            this.lblTotalPillCostXml2.Location = new System.Drawing.Point(-3, 64);
            this.lblTotalPillCostXml2.Name = "lblTotalPillCostXml2";
            this.lblTotalPillCostXml2.Size = new System.Drawing.Size(116, 13);
            this.lblTotalPillCostXml2.TabIndex = 0;
            this.lblTotalPillCostXml2.Text = "Tổng tiền thuốc XML2:";
            // 
            // lblTotalPillCostXml1
            // 
            this.lblTotalPillCostXml1.AutoSize = true;
            this.lblTotalPillCostXml1.Location = new System.Drawing.Point(-3, 48);
            this.lblTotalPillCostXml1.Name = "lblTotalPillCostXml1";
            this.lblTotalPillCostXml1.Size = new System.Drawing.Size(116, 13);
            this.lblTotalPillCostXml1.TabIndex = 0;
            this.lblTotalPillCostXml1.Text = "Tổng tiền thuốc XML1:";
            // 
            // lblTotalPillCostDiff
            // 
            this.lblTotalPillCostDiff.AutoSize = true;
            this.lblTotalPillCostDiff.Location = new System.Drawing.Point(-2, 80);
            this.lblTotalPillCostDiff.Name = "lblTotalPillCostDiff";
            this.lblTotalPillCostDiff.Size = new System.Drawing.Size(64, 13);
            this.lblTotalPillCostDiff.TabIndex = 0;
            this.lblTotalPillCostDiff.Text = "Chênh lệch:";
            // 
            // lblTotalCostDiffXml
            // 
            this.lblTotalCostDiffXml.AutoSize = true;
            this.lblTotalCostDiffXml.Location = new System.Drawing.Point(-2, 32);
            this.lblTotalCostDiffXml.Name = "lblTotalCostDiffXml";
            this.lblTotalCostDiffXml.Size = new System.Drawing.Size(64, 13);
            this.lblTotalCostDiffXml.TabIndex = 0;
            this.lblTotalCostDiffXml.Text = "Chênh lệch:";
            // 
            // lblTotalCostXml23
            // 
            this.lblTotalCostXml23.AutoSize = true;
            this.lblTotalCostXml23.Location = new System.Drawing.Point(-3, 16);
            this.lblTotalCostXml23.Name = "lblTotalCostXml23";
            this.lblTotalCostXml23.Size = new System.Drawing.Size(120, 13);
            this.lblTotalCostXml23.TabIndex = 0;
            this.lblTotalCostXml23.Text = "Tổng tiền XML2, XML3:";
            // 
            // lblTotalCostXml1
            // 
            this.lblTotalCostXml1.AutoSize = true;
            this.lblTotalCostXml1.Location = new System.Drawing.Point(-3, 0);
            this.lblTotalCostXml1.Name = "lblTotalCostXml1";
            this.lblTotalCostXml1.Size = new System.Drawing.Size(86, 13);
            this.lblTotalCostXml1.TabIndex = 0;
            this.lblTotalCostXml1.Text = "Tổng tiền XML1:";
            // 
            // pnlFooter
            // 
            this.pnlFooter.Controls.Add(this.lblTime);
            this.pnlFooter.Controls.Add(this.lblName);
            this.pnlFooter.Location = new System.Drawing.Point(12, 566);
            this.pnlFooter.Name = "pnlFooter";
            this.pnlFooter.Size = new System.Drawing.Size(897, 21);
            this.pnlFooter.TabIndex = 55;
            // 
            // lblTime
            // 
            this.lblTime.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblTime.AutoSize = true;
            this.lblTime.Location = new System.Drawing.Point(732, 4);
            this.lblTime.Name = "lblTime";
            this.lblTime.Size = new System.Drawing.Size(165, 13);
            this.lblTime.TabIndex = 0;
            this.lblTime.Text = "Chủ nhật, 01/01/2017 - 00:00:00";
            this.lblTime.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // lblName
            // 
            this.lblName.AutoSize = true;
            this.lblName.Location = new System.Drawing.Point(-3, 4);
            this.lblName.Name = "lblName";
            this.lblName.Size = new System.Drawing.Size(118, 13);
            this.lblName.TabIndex = 0;
            this.lblName.Text = "Tên bệnh viện - Địa chỉ";
            // 
            // pnlSummary
            // 
            this.pnlSummary.Controls.Add(this.panel3);
            this.pnlSummary.Controls.Add(this.panel6);
            this.pnlSummary.Controls.Add(this.panel4);
            this.pnlSummary.Controls.Add(this.panel5);
            this.pnlSummary.Location = new System.Drawing.Point(12, 462);
            this.pnlSummary.Name = "pnlSummary";
            this.pnlSummary.Size = new System.Drawing.Size(897, 105);
            this.pnlSummary.TabIndex = 50;
            // 
            // tbxSearch
            // 
            this.tbxSearch.Location = new System.Drawing.Point(13, 110);
            this.tbxSearch.Name = "tbxSearch";
            this.tbxSearch.Size = new System.Drawing.Size(137, 20);
            this.tbxSearch.TabIndex = 16;
            this.tbxSearch.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.tbxSearch_KeyPress);
            // 
            // btnSearch
            // 
            this.btnSearch.Location = new System.Drawing.Point(156, 108);
            this.btnSearch.Name = "btnSearch";
            this.btnSearch.Size = new System.Drawing.Size(75, 23);
            this.btnSearch.TabIndex = 17;
            this.btnSearch.Text = "Tìm kiếm";
            this.btnSearch.UseVisualStyleBackColor = true;
            this.btnSearch.Click += new System.EventHandler(this.btnSearch_Click);
            // 
            // btnExport
            // 
            this.btnExport.Location = new System.Drawing.Point(484, 108);
            this.btnExport.Name = "btnExport";
            this.btnExport.Size = new System.Drawing.Size(75, 23);
            this.btnExport.TabIndex = 19;
            this.btnExport.Text = "Xuất dữ liệu";
            this.btnExport.UseVisualStyleBackColor = true;
            // 
            // btnExportVas
            // 
            this.btnExportVas.Location = new System.Drawing.Point(6, 11);
            this.btnExportVas.Name = "btnExportVas";
            this.btnExportVas.Size = new System.Drawing.Size(75, 23);
            this.btnExportVas.TabIndex = 22;
            this.btnExportVas.Text = "Xuất dữ liệu";
            this.btnExportVas.UseVisualStyleBackColor = true;
            // 
            // btnExportWarning
            // 
            this.btnExportWarning.Location = new System.Drawing.Point(6, 11);
            this.btnExportWarning.Name = "btnExportWarning";
            this.btnExportWarning.Size = new System.Drawing.Size(75, 23);
            this.btnExportWarning.TabIndex = 24;
            this.btnExportWarning.Text = "Xuất dữ liệu";
            this.btnExportWarning.UseVisualStyleBackColor = true;
            // 
            // btnExportPaid
            // 
            this.btnExportPaid.Location = new System.Drawing.Point(6, 11);
            this.btnExportPaid.Name = "btnExportPaid";
            this.btnExportPaid.Size = new System.Drawing.Size(75, 23);
            this.btnExportPaid.TabIndex = 26;
            this.btnExportPaid.Text = "Xuất dữ liệu";
            this.btnExportPaid.UseVisualStyleBackColor = true;
            // 
            // btnClearSearch
            // 
            this.btnClearSearch.Location = new System.Drawing.Point(237, 108);
            this.btnClearSearch.Name = "btnClearSearch";
            this.btnClearSearch.Size = new System.Drawing.Size(75, 23);
            this.btnClearSearch.TabIndex = 18;
            this.btnClearSearch.Text = "Xóa tìm kiếm";
            this.btnClearSearch.UseVisualStyleBackColor = true;
            this.btnClearSearch.Click += new System.EventHandler(this.btnClearSearch_Click);
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(924, 599);
            this.Controls.Add(this.btnExport);
            this.Controls.Add(this.btnClearSearch);
            this.Controls.Add(this.btnSearch);
            this.Controls.Add(this.tbxSearch);
            this.Controls.Add(this.pnlSummary);
            this.Controls.Add(this.pnlFooter);
            this.Controls.Add(this.tclError);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.btnViewXml);
            this.Controls.Add(this.grvPatients);
            this.Controls.Add(this.btnImportXml);
            this.Controls.Add(this.menMain);
            this.MainMenuStrip = this.menMain;
            this.MinimumSize = new System.Drawing.Size(940, 638);
            this.Name = "MainForm";
            this.Text = "Tiền Giám Định";
            this.Resize += new System.EventHandler(this.MainForm_Resize);
            ((System.ComponentModel.ISupportInitialize)(this.grvPatients)).EndInit();
            this.menMain.ResumeLayout(false);
            this.menMain.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.tclError.ResumeLayout(false);
            this.tabVas.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.grvVas)).EndInit();
            this.tabWarning.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.grvWarning)).EndInit();
            this.tabPaid.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.grvPaid)).EndInit();
            this.panel6.ResumeLayout(false);
            this.panel6.PerformLayout();
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            this.panel4.ResumeLayout(false);
            this.panel4.PerformLayout();
            this.panel5.ResumeLayout(false);
            this.panel5.PerformLayout();
            this.pnlFooter.ResumeLayout(false);
            this.pnlFooter.PerformLayout();
            this.pnlSummary.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnImportXml;
        private System.Windows.Forms.DataGridView grvPatients;
        private System.ComponentModel.BackgroundWorker bgwReading;
        private System.Windows.Forms.MenuStrip menMain;
        private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem importConfigurationToolStripMenuItem;
        private System.Windows.Forms.Button btnViewXml;
        private System.Windows.Forms.RadioButton rbnFilterAll;
        private System.Windows.Forms.RadioButton rbnFilterAnyError;
        private System.Windows.Forms.RadioButton rbnFilterOk;
        private System.Windows.Forms.RadioButton rbnFilterError;
        private System.Windows.Forms.RadioButton rbnFilterWarning;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.RadioButton rbnAnyPlace;
        private System.Windows.Forms.RadioButton rbnFilterExtenal;
        private System.Windows.Forms.RadioButton rbnFilterAnyExternal;
        private System.Windows.Forms.RadioButton rbnFilterInner;
        private System.Windows.Forms.RadioButton rbnFilterClinic;
        private System.Windows.Forms.TabControl tclError;
        private System.Windows.Forms.TabPage tabVas;
        private System.Windows.Forms.DataGridView grvVas;
        private System.Windows.Forms.TabPage tabWarning;
        private System.Windows.Forms.DataGridView grvWarning;
        private System.Windows.Forms.TabPage tabPaid;
        private System.Windows.Forms.DataGridView grvPaid;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Label lblTotalFile;
        private System.Windows.Forms.Label lblTotalFileValidated;
        private System.Windows.Forms.Label lblTotalFileHasAnyError;
        private System.Windows.Forms.Label lblTotalFileInternal;
        private System.Windows.Forms.Label lblTotalFileExternal;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Label lblTotalCostDiff;
        private System.Windows.Forms.Label lblTotalCostOther;
        private System.Windows.Forms.Label lblTotalCostBntt;
        private System.Windows.Forms.Label lblTotalCostBhtt;
        private System.Windows.Forms.Label lblTotalCost;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.Label lblTotalPillCostXml2;
        private System.Windows.Forms.Label lblTotalPillCostXml1;
        private System.Windows.Forms.Label lblTotalCostDiffXml;
        private System.Windows.Forms.Label lblTotalCostXml23;
        private System.Windows.Forms.Label lblTotalCostXml1;
        private System.Windows.Forms.Label lblTotalPillCostDiff;
        private System.Windows.Forms.Panel panel6;
        private System.Windows.Forms.Label lblTotalEquipCostDiff;
        private System.Windows.Forms.Label lblTotalEquipCostXml2;
        private System.Windows.Forms.Label lblTotalEquipCostXml1;
        private System.Windows.Forms.Panel pnlFooter;
        private System.Windows.Forms.Label lblTime;
        private System.Windows.Forms.Label lblName;
        private System.Windows.Forms.ToolStripMenuItem importMetaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem settingsToolStripMenuItem;
        private System.Windows.Forms.Panel pnlSummary;
        private System.Windows.Forms.TextBox tbxSearch;
        private System.Windows.Forms.Button btnSearch;
        private System.Windows.Forms.Button btnExport;
        private System.Windows.Forms.Button btnExportVas;
        private System.Windows.Forms.Button btnExportWarning;
        private System.Windows.Forms.Button btnExportPaid;
        private System.Windows.Forms.Button btnClearSearch;
    }
}

