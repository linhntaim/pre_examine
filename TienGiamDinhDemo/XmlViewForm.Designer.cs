﻿namespace TienGiamDinhDemo
{
    partial class XmlViewForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tabXml = new System.Windows.Forms.TabControl();
            this.tabXml1 = new System.Windows.Forms.TabPage();
            this.tbxXml1 = new System.Windows.Forms.TextBox();
            this.tabXml2 = new System.Windows.Forms.TabPage();
            this.tbxXml2 = new System.Windows.Forms.TextBox();
            this.tabXml3 = new System.Windows.Forms.TabPage();
            this.tbxXml3 = new System.Windows.Forms.TextBox();
            this.tabXml.SuspendLayout();
            this.tabXml1.SuspendLayout();
            this.tabXml2.SuspendLayout();
            this.tabXml3.SuspendLayout();
            this.SuspendLayout();
            // 
            // tabXml
            // 
            this.tabXml.Controls.Add(this.tabXml1);
            this.tabXml.Controls.Add(this.tabXml2);
            this.tabXml.Controls.Add(this.tabXml3);
            this.tabXml.Location = new System.Drawing.Point(12, 12);
            this.tabXml.Name = "tabXml";
            this.tabXml.SelectedIndex = 0;
            this.tabXml.Size = new System.Drawing.Size(375, 365);
            this.tabXml.TabIndex = 0;
            // 
            // tabXml1
            // 
            this.tabXml1.Controls.Add(this.tbxXml1);
            this.tabXml1.Location = new System.Drawing.Point(4, 22);
            this.tabXml1.Name = "tabXml1";
            this.tabXml1.Padding = new System.Windows.Forms.Padding(3);
            this.tabXml1.Size = new System.Drawing.Size(367, 339);
            this.tabXml1.TabIndex = 0;
            this.tabXml1.Text = "XML1";
            this.tabXml1.UseVisualStyleBackColor = true;
            // 
            // tbxXml1
            // 
            this.tbxXml1.Location = new System.Drawing.Point(6, 6);
            this.tbxXml1.Multiline = true;
            this.tbxXml1.Name = "tbxXml1";
            this.tbxXml1.ReadOnly = true;
            this.tbxXml1.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.tbxXml1.Size = new System.Drawing.Size(355, 327);
            this.tbxXml1.TabIndex = 1;
            // 
            // tabXml2
            // 
            this.tabXml2.Controls.Add(this.tbxXml2);
            this.tabXml2.Location = new System.Drawing.Point(4, 22);
            this.tabXml2.Name = "tabXml2";
            this.tabXml2.Padding = new System.Windows.Forms.Padding(3);
            this.tabXml2.Size = new System.Drawing.Size(367, 339);
            this.tabXml2.TabIndex = 1;
            this.tabXml2.Text = "XML2";
            this.tabXml2.UseVisualStyleBackColor = true;
            // 
            // tbxXml2
            // 
            this.tbxXml2.Location = new System.Drawing.Point(6, 6);
            this.tbxXml2.Multiline = true;
            this.tbxXml2.Name = "tbxXml2";
            this.tbxXml2.ReadOnly = true;
            this.tbxXml2.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.tbxXml2.Size = new System.Drawing.Size(355, 327);
            this.tbxXml2.TabIndex = 2;
            // 
            // tabXml3
            // 
            this.tabXml3.Controls.Add(this.tbxXml3);
            this.tabXml3.Location = new System.Drawing.Point(4, 22);
            this.tabXml3.Name = "tabXml3";
            this.tabXml3.Padding = new System.Windows.Forms.Padding(3);
            this.tabXml3.Size = new System.Drawing.Size(367, 339);
            this.tabXml3.TabIndex = 2;
            this.tabXml3.Text = "XML3";
            this.tabXml3.UseVisualStyleBackColor = true;
            // 
            // tbxXml3
            // 
            this.tbxXml3.Location = new System.Drawing.Point(6, 6);
            this.tbxXml3.Multiline = true;
            this.tbxXml3.Name = "tbxXml3";
            this.tbxXml3.ReadOnly = true;
            this.tbxXml3.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.tbxXml3.Size = new System.Drawing.Size(355, 327);
            this.tbxXml3.TabIndex = 3;
            // 
            // XmlViewForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(399, 389);
            this.Controls.Add(this.tabXml);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "XmlViewForm";
            this.Text = "Xem dữ liệu XML";
            this.tabXml.ResumeLayout(false);
            this.tabXml1.ResumeLayout(false);
            this.tabXml1.PerformLayout();
            this.tabXml2.ResumeLayout(false);
            this.tabXml2.PerformLayout();
            this.tabXml3.ResumeLayout(false);
            this.tabXml3.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl tabXml;
        private System.Windows.Forms.TabPage tabXml1;
        private System.Windows.Forms.TabPage tabXml2;
        private System.Windows.Forms.TabPage tabXml3;
        private System.Windows.Forms.TextBox tbxXml3;
        private System.Windows.Forms.TextBox tbxXml2;
        private System.Windows.Forms.TextBox tbxXml1;
    }
}