﻿namespace TienGiamDinhDemo
{
    partial class ImportMetaForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tbxMetaName = new System.Windows.Forms.TextBox();
            this.grbAdd = new System.Windows.Forms.GroupBox();
            this.btnAdd = new System.Windows.Forms.Button();
            this.grbUpdate = new System.Windows.Forms.GroupBox();
            this.btnUpdate = new System.Windows.Forms.Button();
            this.cbxMeta = new System.Windows.Forms.ComboBox();
            this.btnDrop = new System.Windows.Forms.Button();
            this.grbAdd.SuspendLayout();
            this.grbUpdate.SuspendLayout();
            this.SuspendLayout();
            // 
            // tbxMetaName
            // 
            this.tbxMetaName.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.tbxMetaName.ImeMode = System.Windows.Forms.ImeMode.Alpha;
            this.tbxMetaName.Location = new System.Drawing.Point(6, 19);
            this.tbxMetaName.Name = "tbxMetaName";
            this.tbxMetaName.Size = new System.Drawing.Size(248, 20);
            this.tbxMetaName.TabIndex = 1;
            this.tbxMetaName.KeyUp += new System.Windows.Forms.KeyEventHandler(this.tbxMetaName_KeyUp);
            // 
            // grbAdd
            // 
            this.grbAdd.Controls.Add(this.btnAdd);
            this.grbAdd.Controls.Add(this.tbxMetaName);
            this.grbAdd.Location = new System.Drawing.Point(12, 12);
            this.grbAdd.Name = "grbAdd";
            this.grbAdd.Size = new System.Drawing.Size(260, 82);
            this.grbAdd.TabIndex = 1;
            this.grbAdd.TabStop = false;
            this.grbAdd.Text = "Danh mục mới";
            // 
            // btnAdd
            // 
            this.btnAdd.Enabled = false;
            this.btnAdd.Location = new System.Drawing.Point(179, 45);
            this.btnAdd.Name = "btnAdd";
            this.btnAdd.Size = new System.Drawing.Size(75, 23);
            this.btnAdd.TabIndex = 2;
            this.btnAdd.Text = "Nhập mới";
            this.btnAdd.UseVisualStyleBackColor = true;
            this.btnAdd.Click += new System.EventHandler(this.btnAdd_Click);
            // 
            // grbUpdate
            // 
            this.grbUpdate.Controls.Add(this.btnDrop);
            this.grbUpdate.Controls.Add(this.btnUpdate);
            this.grbUpdate.Controls.Add(this.cbxMeta);
            this.grbUpdate.Location = new System.Drawing.Point(12, 100);
            this.grbUpdate.Name = "grbUpdate";
            this.grbUpdate.Size = new System.Drawing.Size(260, 83);
            this.grbUpdate.TabIndex = 2;
            this.grbUpdate.TabStop = false;
            this.grbUpdate.Text = "Danh mục cũ";
            // 
            // btnUpdate
            // 
            this.btnUpdate.Location = new System.Drawing.Point(179, 46);
            this.btnUpdate.Name = "btnUpdate";
            this.btnUpdate.Size = new System.Drawing.Size(75, 23);
            this.btnUpdate.TabIndex = 4;
            this.btnUpdate.Text = "Dùng lại";
            this.btnUpdate.UseVisualStyleBackColor = true;
            this.btnUpdate.Click += new System.EventHandler(this.btnUpdate_Click);
            // 
            // cbxMeta
            // 
            this.cbxMeta.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbxMeta.FormattingEnabled = true;
            this.cbxMeta.Location = new System.Drawing.Point(6, 19);
            this.cbxMeta.Name = "cbxMeta";
            this.cbxMeta.Size = new System.Drawing.Size(248, 21);
            this.cbxMeta.TabIndex = 3;
            // 
            // btnDrop
            // 
            this.btnDrop.Location = new System.Drawing.Point(6, 46);
            this.btnDrop.Name = "btnDrop";
            this.btnDrop.Size = new System.Drawing.Size(75, 23);
            this.btnDrop.TabIndex = 5;
            this.btnDrop.Text = "Xóa";
            this.btnDrop.UseVisualStyleBackColor = true;
            this.btnDrop.Click += new System.EventHandler(this.btnDrop_Click);
            // 
            // ImportMetaForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(284, 195);
            this.Controls.Add(this.grbUpdate);
            this.Controls.Add(this.grbAdd);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "ImportMetaForm";
            this.Text = "Nhập danh mục";
            this.grbAdd.ResumeLayout(false);
            this.grbAdd.PerformLayout();
            this.grbUpdate.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TextBox tbxMetaName;
        private System.Windows.Forms.GroupBox grbAdd;
        private System.Windows.Forms.Button btnAdd;
        private System.Windows.Forms.GroupBox grbUpdate;
        private System.Windows.Forms.Button btnUpdate;
        private System.Windows.Forms.ComboBox cbxMeta;
        private System.Windows.Forms.Button btnDrop;
    }
}