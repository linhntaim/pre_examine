﻿using MetaConnect;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.OleDb;
using System.Drawing;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Windows.Forms;
using XmlImport;

namespace TienGiamDinhDemo
{
    public partial class ImportMetaForm : Form
    {
        private List<string> meta;
        private Connector metaConnector;
        private Manager manager;

        public ImportMetaForm(Connector metaConnector, Manager manager)
        {
            InitializeComponent();

            this.metaConnector = metaConnector;
            this.manager = manager;

            UpdateMetaComboBox();
        }

        private void UpdateMetaComboBox()
        {
            this.meta = new List<string>();
            this.meta.AddRange(manager.GetMeta());

            cbxMeta.Items.Clear();
            foreach (string metaItem in this.meta)
            {
                cbxMeta.Items.Add(metaItem);
            }
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            string metaName = tbxMetaName.Text.Trim();
            if (metaName == "")
            {
                MessageBox.Show("Hãy nhập tên danh mục mới");
                return;
            }

            if (meta.Contains(metaName))
            {
                MessageBox.Show("Tên danh mục mới không được trùng với tên danh mục cũ");
                return;
            }

            OpenFile(metaName);
        }

        private void btnUpdate_Click(object sender, EventArgs e)
        {
            if (cbxMeta.SelectedItem == null)
            {
                MessageBox.Show("Hãy chọn danh mục đã có");
                return;
            }
            OpenFile(cbxMeta.SelectedItem.ToString());
        }

        private void OpenFile(string table)
        {
            OpenFileDialog dialog = new OpenFileDialog();
            dialog.Filter = "Tập tin CSV (*.csv)|*.csv";
            dialog.Title = "Chọn một tập tin CSV để nhập";
            if (dialog.ShowDialog() == DialogResult.OK)
            {
                Import(dialog.FileName, table);
            }
        }

        private void Import(string fileName, string tableName)
        {
            string path = Path.GetDirectoryName(fileName);
            string file = Path.GetFileName(fileName);

            string sql = @"SELECT * FROM [" + file + "]";

            using (OleDbConnection connection = new OleDbConnection(
                      @"Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + path +
                      ";Extended Properties=\"Text;HDR=Yes;CharacterSet=65001;\""))
            using (OleDbCommand command = new OleDbCommand(sql, connection))
            using (OleDbDataAdapter adapter = new OleDbDataAdapter(command))
            {
                DataTable dataTable = new DataTable(tableName);
                adapter.Fill(dataTable);

                Result result = metaConnector.GenerateTable(dataTable);
                if (result is OkResult)
                {
                    manager.SaveMeta(tableName);

                    tbxMetaName.Text = "";

                    UpdateMetaComboBox();

                    MessageBox.Show("Nhập danh mục thành công");
                }
                else
                {
                    MessageBox.Show("Lỗi: " + result.GetMessage());
                }
            }
        }

        private void tbxMetaName_KeyUp(object sender, KeyEventArgs e)
        {
            if (!Regex.IsMatch(tbxMetaName.Text, "^[0-9A-Z_]*$"))
            {
                btnAdd.Enabled = false;
                MessageBox.Show("Chỉ chấp nhận các ký tự chữ số (0-9), chữ cái (A-Z) và dấu gạch chân (_)");

                tbxMetaName.Text = Regex.Replace(tbxMetaName.Text, "[^0-9A-Z_]", "_");

                tbxMetaName.SelectionStart = tbxMetaName.Text.Length;
                tbxMetaName.SelectionLength = 0;
            }

            btnAdd.Enabled = tbxMetaName.Text.Trim() != "";
        }

        private void btnDrop_Click(object sender, EventArgs e)
        {
            string tableName = cbxMeta.SelectedItem.ToString();
            Result result = metaConnector.DropTable(tableName);
            if (result is OkResult)
            {
                manager.DeleteMeta(tableName);

                UpdateMetaComboBox();

                MessageBox.Show("Xóa thành công");
            }
            else
            {
                MessageBox.Show("Lỗi: " + result.GetMessage());
            }
        }
    }
}
