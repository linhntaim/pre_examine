﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using XmlImport;

namespace TienGiamDinhDemo
{
    public partial class SettingsForm : Form
    {
        private Manager manager;

        public SettingsForm(Manager manager)
        {
            InitializeComponent();

            this.manager = manager;
            btnSave.DialogResult = DialogResult.OK;
            btnCancel.DialogResult = DialogResult.Cancel;
            tbxHospitalCode.Text = manager.Settings.HospitalCode;
            tbxHospitalName.Text = manager.Settings.HospitalName;
            tbxHospitalAddress.Text = manager.Settings.HospitalAddress;
            tbxCsKcbCode.Text = manager.Settings.CsKcbCode;
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            manager.Settings.HospitalCode = tbxHospitalCode.Text;
            manager.Settings.HospitalName = tbxHospitalName.Text;
            manager.Settings.HospitalAddress = tbxHospitalAddress.Text;
            manager.Settings.CsKcbCode = tbxCsKcbCode.Text;
            manager.SaveImportConfiguration();

            this.Close();
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
