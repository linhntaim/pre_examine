﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace TienGiamDinhDemo
{
    public partial class XmlViewForm : Form
    {
        public XmlViewForm(Dictionary<string, string> xml)
        {
            InitializeComponent();

            tbxXml1.Text = xml.ContainsKey("XML1") ? xml["XML1"] : "";
            tbxXml2.Text = xml.ContainsKey("XML2") ? xml["XML2"] : "";
            tbxXml3.Text = xml.ContainsKey("XML3") ? xml["XML3"] : "";
        }
    }
}
