﻿namespace TienGiamDinhDemo
{
    partial class SettingsForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblHospitalCode = new System.Windows.Forms.Label();
            this.tbxHospitalCode = new System.Windows.Forms.TextBox();
            this.lblHospitalName = new System.Windows.Forms.Label();
            this.tbxHospitalName = new System.Windows.Forms.TextBox();
            this.lblHospitalAddress = new System.Windows.Forms.Label();
            this.tbxHospitalAddress = new System.Windows.Forms.TextBox();
            this.lblCsKcbCode = new System.Windows.Forms.Label();
            this.tbxCsKcbCode = new System.Windows.Forms.TextBox();
            this.btnSave = new System.Windows.Forms.Button();
            this.btnCancel = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // lblHospitalCode
            // 
            this.lblHospitalCode.AutoSize = true;
            this.lblHospitalCode.Location = new System.Drawing.Point(9, 9);
            this.lblHospitalCode.Name = "lblHospitalCode";
            this.lblHospitalCode.Size = new System.Drawing.Size(72, 13);
            this.lblHospitalCode.TabIndex = 0;
            this.lblHospitalCode.Text = "Mã bệnh viện";
            // 
            // tbxHospitalCode
            // 
            this.tbxHospitalCode.Location = new System.Drawing.Point(12, 25);
            this.tbxHospitalCode.Name = "tbxHospitalCode";
            this.tbxHospitalCode.Size = new System.Drawing.Size(307, 20);
            this.tbxHospitalCode.TabIndex = 1;
            // 
            // lblHospitalName
            // 
            this.lblHospitalName.AutoSize = true;
            this.lblHospitalName.Location = new System.Drawing.Point(9, 52);
            this.lblHospitalName.Name = "lblHospitalName";
            this.lblHospitalName.Size = new System.Drawing.Size(76, 13);
            this.lblHospitalName.TabIndex = 0;
            this.lblHospitalName.Text = "Tên bệnh viện";
            // 
            // tbxHospitalName
            // 
            this.tbxHospitalName.Location = new System.Drawing.Point(12, 68);
            this.tbxHospitalName.Name = "tbxHospitalName";
            this.tbxHospitalName.Size = new System.Drawing.Size(307, 20);
            this.tbxHospitalName.TabIndex = 2;
            // 
            // lblHospitalAddress
            // 
            this.lblHospitalAddress.AutoSize = true;
            this.lblHospitalAddress.Location = new System.Drawing.Point(9, 95);
            this.lblHospitalAddress.Name = "lblHospitalAddress";
            this.lblHospitalAddress.Size = new System.Drawing.Size(90, 13);
            this.lblHospitalAddress.TabIndex = 0;
            this.lblHospitalAddress.Text = "Địa chỉ bệnh viện";
            // 
            // tbxHospitalAddress
            // 
            this.tbxHospitalAddress.Location = new System.Drawing.Point(12, 111);
            this.tbxHospitalAddress.Name = "tbxHospitalAddress";
            this.tbxHospitalAddress.Size = new System.Drawing.Size(307, 20);
            this.tbxHospitalAddress.TabIndex = 3;
            // 
            // lblCsKcbCode
            // 
            this.lblCsKcbCode.AutoSize = true;
            this.lblCsKcbCode.Location = new System.Drawing.Point(9, 140);
            this.lblCsKcbCode.Name = "lblCsKcbCode";
            this.lblCsKcbCode.Size = new System.Drawing.Size(75, 13);
            this.lblCsKcbCode.TabIndex = 0;
            this.lblCsKcbCode.Text = "Mã cơ sở KCB";
            // 
            // tbxCsKcbCode
            // 
            this.tbxCsKcbCode.Location = new System.Drawing.Point(12, 156);
            this.tbxCsKcbCode.Name = "tbxCsKcbCode";
            this.tbxCsKcbCode.Size = new System.Drawing.Size(307, 20);
            this.tbxCsKcbCode.TabIndex = 4;
            // 
            // btnSave
            // 
            this.btnSave.Location = new System.Drawing.Point(244, 182);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(75, 23);
            this.btnSave.TabIndex = 5;
            this.btnSave.Text = "Lưu";
            this.btnSave.UseVisualStyleBackColor = true;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // btnCancel
            // 
            this.btnCancel.Location = new System.Drawing.Point(12, 182);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(75, 23);
            this.btnCancel.TabIndex = 6;
            this.btnCancel.Text = "Hủy";
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // SettingsForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(331, 218);
            this.ControlBox = false;
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.btnSave);
            this.Controls.Add(this.tbxCsKcbCode);
            this.Controls.Add(this.lblCsKcbCode);
            this.Controls.Add(this.tbxHospitalAddress);
            this.Controls.Add(this.lblHospitalAddress);
            this.Controls.Add(this.tbxHospitalName);
            this.Controls.Add(this.lblHospitalName);
            this.Controls.Add(this.tbxHospitalCode);
            this.Controls.Add(this.lblHospitalCode);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "SettingsForm";
            this.Text = "Thiết lập";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblHospitalCode;
        private System.Windows.Forms.TextBox tbxHospitalCode;
        private System.Windows.Forms.Label lblHospitalName;
        private System.Windows.Forms.TextBox tbxHospitalName;
        private System.Windows.Forms.Label lblHospitalAddress;
        private System.Windows.Forms.TextBox tbxHospitalAddress;
        private System.Windows.Forms.Label lblCsKcbCode;
        private System.Windows.Forms.TextBox tbxCsKcbCode;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.Button btnCancel;
    }
}