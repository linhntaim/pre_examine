﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using XmlImport;
using MetaConnect;

namespace TienGiamDinhDemo
{
    public partial class MainForm : Form
    {
        private ReadingForm readingForm;
        private Manager manager;
        private Connector metaConnector;

        private int currentFilterErrorLevel = -1;
        private int currentFilterPlace = -1;
        private string currentFilterSearch = "";

        private int l_padding;
        private int l_between;
        private int l_lower;
        private int l_lower_tab;

        public MainForm()
        {
            InitializeComponent();

            l_padding = btnImportXml.Location.X;
            l_between = tclError.Location.X - l_padding - grvPatients.Width;
            l_lower = pnlSummary.Location.Y - (grvPatients.Location.Y + grvPatients.Height);
            l_lower_tab = pnlSummary.Location.Y - l_lower - (grvVas.Location.Y + grvVas.Height);

            UpdateLayout();

            metaConnector = new Connector();
            manager = new Manager();
            manager.LoadConfiguration();

            UpdateFooter();
        }

        protected void UpdateFooter()
        {
            lblName.Text = manager.Settings.HospitalName + " - " + manager.Settings.HospitalAddress;

            UpdateDate();
        }

        private void importConfigurationToolStripMenuItem_Click(object sender, EventArgs e)
        {
            OpenFileDialog dialog = new OpenFileDialog();
            dialog.Filter = "Tập tin XML (*.xml)|*.xml";
            dialog.Title = "Chọn một tập tin XML để nhập";
            if (dialog.ShowDialog() == DialogResult.OK)
            {
                ImportConfiguration(dialog.FileName);

                UpdateDate();
            }
        }

        private void ImportConfiguration(string fileName)
        {
            if (manager.LoadConfiguration(fileName))
            {
                MessageBox.Show("Nhập cấu hình thành công");
            }
            else
            {
                MessageBox.Show("Nhập cấu hình thất bại");
            }
        }

        private void importMetaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ImportMetaForm form = new ImportMetaForm(metaConnector, manager);
            if (form.ShowDialog(this) == DialogResult.OK)
            {
                UpdateDate();
            }
            form.Dispose();
        }

        private void settingsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            SettingsForm form = new SettingsForm(manager);
            if (form.ShowDialog(this) == DialogResult.OK)
            {
                UpdateFooter();
            }
            form.Dispose();
        }

        private void btnImportXml_Click(object sender, EventArgs e)
        {
            OpenFileDialog dialog = new OpenFileDialog();
            dialog.Filter = "Tập tin XML (*.xml)|*.xml";
            dialog.Multiselect = true;
            dialog.Title = "Chọn một hoặc nhiều tập tin XML để nhập";
            if (dialog.ShowDialog() == DialogResult.OK)
            {
                if (dialog.FileNames.Length > 0)
                {
                    this.StartImporting(dialog.FileNames);
                }
            }
        }

        private void StartImporting(string[] fileNames)
        {
            manager.StartNewSession();

            if (!bgwReading.IsBusy)
            {
                readingForm = new ReadingForm();
                readingForm.Cancelled += new EventHandler<EventArgs>(btnCancelImporting_Click);
                readingForm.Show(this);

                bgwReading.RunWorkerAsync(fileNames);
            }
        }

        private void btnCancelImporting_Click(object sender, EventArgs e)
        {
            if (bgwReading.WorkerSupportsCancellation == true)
            {
                bgwReading.CancelAsync();

                readingForm.Close();
            }
        }

        private void bgwReading_DoWork(object sender, DoWorkEventArgs e)
        {
            BackgroundWorker worker = sender as BackgroundWorker;

            string[] fileNames = e.Argument as string[];
            for (int i = 0, loop = fileNames.Length; i < loop; ++i)
            {
                if (worker.CancellationPending == true)
                {
                    e.Cancel = true;
                    break;
                }
                else
                {
                    manager.Push(fileNames[i]);

                    worker.ReportProgress((int)((float)(i + 1) / loop * 100));
                }
            }
        }

        private void bgwReading_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            readingForm.Percent = e.ProgressPercentage;
        }

        private void bgwReading_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            readingForm.Close();
            readingForm.Dispose();

            if (e.Cancelled == true)
            {
                MessageBox.Show("Hủy!");
            }
            else if (e.Error != null)
            {
                MessageBox.Show("Lỗi: " + e.Error.Message);
            }
            else
            {
                MessageBox.Show("Hoàn tất!");

                rbnFilterAll.Checked = true;
                rbnAnyPlace.Checked = true;
                ShowData();
                ShowValidation();
                ShowSummary();
            }
        }

        private void ShowData()
        {
            DataTable dt = new DataTable();
            string[] columns = manager.GetHeader();
            foreach (string column in columns)
            {
                dt.Columns.Add(column);
            }
            string[][] rows = manager.GetRows(currentFilterErrorLevel, currentFilterPlace, currentFilterSearch);
            foreach (string[] row in rows)
            {
                dt.Rows.Add(row);
            }
            grvPatients.DataSource = dt;
        }

        private void ShowValidation()
        {
            grvVas.DataSource = manager.GetErrorVas();
            grvWarning.DataSource = manager.GetErrorWarning();
            grvPaid.DataSource = manager.GetErrorPaid();
        }

        private void ShowSummary()
        {
            lblTotalFile.Text = lblTotalFile.Text.Split(':')[0] + ": " + manager.Summary.TotalFile;
            lblTotalFileValidated.Text = lblTotalFileValidated.Text.Split(':')[0] + ": " + manager.Summary.TotalFileValidated;
            lblTotalFileHasAnyError.Text = lblTotalFileHasAnyError.Text.Split(':')[0] + ": " + manager.Summary.TotalFileHasAnyError;
            lblTotalFileInternal.Text = lblTotalFileInternal.Text.Split(':')[0] + ": " + manager.Summary.TotalFileInternal;
            lblTotalFileExternal.Text = lblTotalFileExternal.Text.Split(':')[0] + ": " + manager.Summary.TotalFileExternal;

            lblTotalCost.Text = lblTotalCost.Text.Split(':')[0] + ": " + manager.Summary.TotalCost;
            lblTotalCostBhtt.Text = lblTotalCostBhtt.Text.Split(':')[0] + ": " + manager.Summary.TotalCostBhtt;
            lblTotalCostBntt.Text = lblTotalCostBntt.Text.Split(':')[0] + ": " + manager.Summary.TotalCostBntt;
            lblTotalCostOther.Text = lblTotalCostOther.Text.Split(':')[0] + ": " + manager.Summary.TotalCostOther;
            lblTotalCostDiff.Text = lblTotalCostDiff.Text.Split(':')[0] + ": " + manager.Summary.TotalCostDiff;

            lblTotalCostXml1.Text = lblTotalCostXml1.Text.Split(':')[0] + ": " + manager.Summary.TotalCostXml1;
            lblTotalCostXml23.Text = lblTotalCostXml23.Text.Split(':')[0] + ": " + manager.Summary.TotalCostXml23;
            lblTotalCostDiffXml.Text = lblTotalCostDiffXml.Text.Split(':')[0] + ": " + manager.Summary.TotalCostDiffXml;
            lblTotalPillCostXml1.Text = lblTotalPillCostXml1.Text.Split(':')[0] + ": " + manager.Summary.TotalPillCostXml1;
            lblTotalPillCostXml2.Text = lblTotalPillCostXml2.Text.Split(':')[0] + ": " + manager.Summary.TotalPillCostXml2;
            lblTotalPillCostDiff.Text = lblTotalPillCostDiff.Text.Split(':')[0] + ": " + manager.Summary.TotalPillCostDiff;

            lblTotalEquipCostXml1.Text = lblTotalEquipCostXml1.Text.Split(':')[0] + ": " + manager.Summary.TotalEquipCostXml1;
            lblTotalEquipCostXml2.Text = lblTotalEquipCostXml2.Text.Split(':')[0] + ": " + manager.Summary.TotalEquipCostXml3;
            lblTotalEquipCostDiff.Text = lblTotalEquipCostDiff.Text.Split(':')[0] + ": " + manager.Summary.TotalEquipCostDiff;
        }

        private void btnViewXml_Click(object sender, EventArgs e)
        {
            if (grvPatients.SelectedRows.Count > 0)
            {
                XmlViewForm xmlViewForm = new XmlViewForm(manager.GetPatientDataXml(grvPatients.SelectedRows[0].Index));
                if (xmlViewForm.ShowDialog(this) == DialogResult.OK)
                {

                }
                xmlViewForm.Dispose();
            }
        }

        private void rbnFilterAll_CheckedChanged(object sender, EventArgs e)
        {
            currentFilterErrorLevel = -1;

            ShowData();
        }

        private void rbnFilterOk_CheckedChanged(object sender, EventArgs e)
        {
            currentFilterErrorLevel = 0;

            ShowData();
        }

        private void rbnFilterAnyError_CheckedChanged(object sender, EventArgs e)
        {
            currentFilterErrorLevel = 3;

            ShowData();
        }

        private void rbnFilterError_CheckedChanged(object sender, EventArgs e)
        {
            currentFilterErrorLevel = 1;

            ShowData();
        }

        private void rbnFilterWarning_CheckedChanged(object sender, EventArgs e)
        {
            currentFilterErrorLevel = 2;

            ShowData();
        }

        private void rbnAnyPlace_CheckedChanged(object sender, EventArgs e)
        {
            currentFilterPlace = -1;

            ShowData();
        }

        private void rbnFilterInner_CheckedChanged(object sender, EventArgs e)
        {
            currentFilterPlace = 3;

            ShowData();
        }

        private void rbnFilterAnyExternal_CheckedChanged(object sender, EventArgs e)
        {
            currentFilterPlace = 4;

            ShowData();
        }

        private void rbnFilterClinic_CheckedChanged(object sender, EventArgs e)
        {
            currentFilterPlace = 1;

            ShowData();
        }

        private void rbnFilterExtenal_CheckedChanged(object sender, EventArgs e)
        {
            currentFilterPlace = 2;

            ShowData();
        }

        private void UpdateDate()
        {
            DateTime now = manager.UpdateAt;
            string day = "Thứ hai";
            switch (now.DayOfWeek)
            {
                case DayOfWeek.Monday:
                    day = "Thứ hai";
                    break;
                case DayOfWeek.Tuesday:
                    day = "Thứ ba";
                    break;
                case DayOfWeek.Wednesday:
                    day = "Thứ tư";
                    break;
                case DayOfWeek.Thursday:
                    day = "Thứ năm";
                    break;
                case DayOfWeek.Friday:
                    day = "Thứ sáu";
                    break;
                case DayOfWeek.Saturday:
                    day = "Thứ bảy";
                    break;
                case DayOfWeek.Sunday:
                    day = "Chủ nhật";
                    break;
            }
            lblTime.Text = day + ", " + now.ToString("dd'/'MM'/'yyyy - HH:mm:ss");
        }

        private void MainForm_Resize(object sender, EventArgs e)
        {
            UpdateLayout();
        }

        private void UpdateLayout()
        {
            pnlFooter.Location = new Point(pnlFooter.Location.X, this.ClientSize.Height - l_padding - pnlFooter.Height);
            pnlFooter.Width = ClientSize.Width - 2 * l_padding;
            pnlSummary.Location = new Point(pnlSummary.Location.X, this.ClientSize.Height - l_padding - pnlFooter.Height - pnlSummary.Height);

            tclError.Location = new Point(this.ClientSize.Width - l_padding - tclError.Width, tclError.Location.Y);
            grvPatients.Width = tclError.Location.X - l_padding - l_between;

            grvVas.Height = grvWarning.Height = grvPaid.Height = pnlSummary.Location.Y - l_lower - l_lower_tab - grvVas.Location.Y;
            tclError.Height = pnlSummary.Location.Y - l_lower - tclError.Location.Y;
            grvPatients.Height = pnlSummary.Location.Y - l_lower - grvPatients.Location.Y;

            btnExport.Location = new Point(grvPatients.Location.X + grvPatients.Width - btnExport.Width, btnExport.Location.Y);
        }

        private void btnSearch_Click(object sender, EventArgs e)
        {
            DoSearch();
        }

        private void tbxSearch_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
            {
                DoSearch();
            }
        }

        private void btnClearSearch_Click(object sender, EventArgs e)
        {
            tbxSearch.Text = "";
            DoSearch();
        }

        private void DoSearch()
        {
            currentFilterSearch = tbxSearch.Text;

            ShowData();
        }

        protected override bool ProcessCmdKey(ref Message msg, Keys keyData)
        {
            if (keyData == (Keys.Control | Keys.F))
            {
                tbxSearch.Focus();
                return true;
            }
            return base.ProcessCmdKey(ref msg, keyData);
        }
    }
}
