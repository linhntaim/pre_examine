﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace TienGiamDinhDemo
{
    public partial class ReadingForm : Form
    {
        public event EventHandler<EventArgs> Cancelled;

        public int Percent
        {
            set {
                lblPercent.Text = value.ToString() + '%';
                prbReading.Value = value;
                prbReading.Update();
            }
        }

        public ReadingForm()
        {
            InitializeComponent();
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            Cancelled?.Invoke(this, e);
        }
    }
}
