﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SQLite;
using System.Linq;
using System.Text;

namespace MetaConnect
{
    public class Connector
    {
        private static string META_FILE = AppDomain.CurrentDomain.BaseDirectory + "/meta.mem";
        private SQLiteConnection connection;

        public Connector()
        {
            connection = new SQLiteConnection(@"Data Source=" + META_FILE);
        }

        public Result GenerateTable(DataTable table)
        {
            connection.Open();
            Result result = new OkResult();
            try
            {
                using (var cmd = new SQLiteCommand(connection))
                {
                    using (var transaction = connection.BeginTransaction())
                    {
                        cmd.CommandText = "DROP TABLE IF EXISTS " + table.TableName + ";";
                        cmd.ExecuteNonQuery();

                        List<string> fields = new List<string>();
                        List<string> definedFields = new List<string>();
                        foreach (DataColumn column in table.Columns)
                        {
                            definedFields.Add(column.ColumnName + " NVARCHAR(255) NULL");
                            fields.Add(column.ColumnName);
                        }
                        cmd.CommandText = "CREATE TABLE " + table.TableName + " (" + string.Join(", ", definedFields.ToArray()) + ");";
                        cmd.ExecuteNonQuery();
                        List<string> values = new List<string>();
                        foreach (DataRow row in table.Rows)
                        {
                            values.Clear();
                            foreach (object item in row.ItemArray)
                            {
                                values.Add(item.ToString().Replace("'", "''"));
                            }
                            cmd.CommandText = "INSERT INTO " + table.TableName + " (" + string.Join(", ", fields.ToArray()) + ") VALUES ('" + string.Join("', '", values.ToArray()) + "');";
                            try
                            {
                                cmd.ExecuteNonQuery();
                            }
                            catch (Exception ex)
                            {
                                throw ex;
                            }
                        }

                        transaction.Commit();
                    }
                }
            }
            catch (Exception ex)
            {
                result = new FailedResult(ex.Message);
            }
            finally
            {
                connection.Close();
            }

            return result;
        }

        public Result DropTable(string tableName)
        {
            connection.Open();
            Result result = new OkResult();
            try
            {
                using (var cmd = new SQLiteCommand(connection))
                {
                    cmd.CommandText = "DROP TABLE IF EXISTS " + tableName + ";";
                    cmd.ExecuteNonQuery();
                }
            }
            catch (Exception ex)
            {
                result = new FailedResult(ex.Message);
            }
            finally
            {
                connection.Close();
            }

            return result;
        }

        public bool Existed(string tableName, List<string[]> wheres)
        {
            connection.Open();
            try
            {
                using (var cmd = new SQLiteCommand(connection))
                {
                    List<string> whereList = new List<string>();
                    foreach (string[] where in wheres)
                    {
                        if (where.Length == 2)
                        {
                            whereList.Add(where[0] + "=" + where[1]);
                        }
                        else if (where.Length == 3)
                        {
                            whereList.Add(where[0] + where[1] + where[2]);
                        }
                    }
                    cmd.CommandText = "SELECT COUNT(*) as TOTAL FROM " + tableName + " where " + string.Join(" and ", whereList.ToArray());

                    using (SQLiteDataReader rdr = cmd.ExecuteReader())
                    {
                        rdr.Read();
                        return int.Parse(rdr["TOTAL"].ToString()) > 0;
                    }
                }
            }
            catch (Exception ex)
            {
            }
            finally
            {
                connection.Close();
            }

            return false;
        }

        public bool NotExisted(string tableName, List<string[]> wheres)
        {
            connection.Open();
            try
            {
                using (var cmd = new SQLiteCommand(connection))
                {
                    List<string> whereList = new List<string>();
                    foreach (string[] where in wheres)
                    {
                        if (where.Length == 2)
                        {
                            whereList.Add(where[0] + "=" + where[1]);
                        }
                        else if (where.Length == 3)
                        {
                            whereList.Add(where[0] + where[1] + where[2]);
                        }
                    }
                    cmd.CommandText = "SELECT COUNT(*) as TOTAL FROM " + tableName + " where " + string.Join(" and ", whereList.ToArray());

                    using (SQLiteDataReader rdr = cmd.ExecuteReader())
                    {
                        rdr.Read();
                        return int.Parse(rdr["TOTAL"].ToString()) == 0;
                    }
                }
            }
            catch (Exception ex)
            {
            }
            finally
            {
                connection.Close();
            }

            return false;
        }

        ~Connector()
        {
            connection.Dispose();
        }
    }
}
