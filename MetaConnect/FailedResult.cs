﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MetaConnect
{
    public class FailedResult : Result
    {
        public FailedResult(string message) : base(message) { }
    }
}
