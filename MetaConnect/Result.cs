﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MetaConnect
{
    public class Result
    {
        protected string message;
        public Result(string message = null)
        {
            this.message = message;
        }

        public string GetMessage()
        {
            return message;
        }
    }
}
